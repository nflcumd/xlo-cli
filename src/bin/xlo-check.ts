#!/usr/bin/env node

// import devnull from 'dev-null';
import * as fs from 'fs-extra';
import yauzl, { ZipFile } from 'yauzl';
import { LoPackageExporter, XloStateEnum } from '../index';
import * as path from 'path';

const lpe: LoPackageExporter = new LoPackageExporter();

function promisify(api: any) {
  return (...args: any[]) => {
    return new Promise((resolve, reject) => {
      api(...args, (err: any, response: any) => {
        if (err) {
          return reject(err);
        }
        resolve(response);
      });
    });
  };
}

const yauzlOpen = promisify(yauzl.open);

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
  .description(
    'This will check the integrity of the zip archive and match the expected number of data files against those found the /data directory of the zip file.',
  )
  .option('-f, --failfast', 'Fail on first error')
  .parse(process.argv);

const state: XloStateEnum = lpe.checkDir(process.cwd());
const errors: any = [];

if (state !== XloStateEnum.READYTOPACK) {
  log(chalk`
You must first run:

    {magenta xlo init [dir]}
`);
} else {
  (async () => {
    const dir: string = process.cwd();
    const packageDirName = path.basename(dir);
    const list: any = lpe.getFilterListJson();
    if (!list) {
      log(chalk`\t{red No filterList.json file found}`);
    }
    const pkgType = { identified: false }
    for (const lo of list) {
      // package created under folder of same name as parent folder
      await checkZipFile(path.join(packageDirName, lo.id + '.zip'), lo.fileCount, !!lo.pdfReactorJobId, pkgType).catch(err => {
        if (program.failfast) {
          log(chalk`\t{red ${err.message}}`);
          process.exit(1);
        } else {
          errors.push(err.message);
          return Promise.resolve();
        }
      });
    }
  })()
    .then(() => {
      if (errors.length) {
        log(chalk`{red *** Errors Found ***}`);
        errors.map((x: string) => log(chalk`{red ${x}}`));
        // beep sound
        console.log("\u0007"); console.log("\u0007");
        setTimeout(() => process.exit(1), 500);
      } else {
        console.log("\u0007"); console.log("\u0007");
        setTimeout(() => process.exit(), 500);
      }
    })
    .catch(err => {
      log(err);
      process.exit(1);
    });
}

async function checkZipFile(file: string, fileCount: number, hasPDFReactorJob: boolean, pkgType: { identified: boolean }) {
  const zipfile = (await yauzlOpen(file, { lazyEntries: true, validateEntrySizes: true })) as ZipFile;
  let dataFileCount = 0;
  return new Promise<void>((resolve, reject) => {
    log(chalk`Checking: {magenta ${file}}`);
    let isCompressed = false;
    const openReadStream = promisify(zipfile.openReadStream.bind(zipfile));
    zipfile.readEntry();
    zipfile.on('entry', async entry => {
      if (/\/$/.test(entry.fileName) === false) {
        if (/^data\//.test(entry.fileName)) {
          ++dataFileCount;
        }
        if (!pkgType.identified) {
          const basename = entry.fileName.split('/').pop();
          if (basename === 'imscp_v1p1.xsd') {
            log(chalk`\tPackage type: {magenta SCORM 2004}`);
            pkgType.identified = true;
          } else if (basename === 'imscp_rootv1p1p2.xsd') {
            log(chalk`\tPackage type: {magenta SCORM 1.2}`);
            pkgType.identified = true;
          } else if (basename === 'tincan.xml') {
            log(chalk`\tPackage type: {magenta TinCan}`);
            pkgType.identified = true;
          }
        }
        if (entry.isCompressed()) {
          isCompressed = true;
          const stream: any = await openReadStream(entry, { decompress: null });
          stream.on('end', () => {
            zipfile.readEntry();
          });
          stream.on('error', (e: any) => {
            reject(new Error(`found in ${entry.fileName}`));
          });
          stream.pipe(fs.createWriteStream('/dev/null'));
          // stream.pipe(devnull());
        } else {
          zipfile.readEntry();
        }
      } else {
        zipfile.readEntry();
      }
    });
    zipfile.on('end', () => {

      if (isCompressed) {
        log(chalk`\tZip archive integrity: {magenta OK}`);
      }
      resolve();
    });
    zipfile.on('error', () => {
      reject(new Error(`Error found in ${file}`));
    });
  }).then(() => {
    if (hasPDFReactorJob && (dataFileCount + 1) === fileCount) {
      log(chalk`\t{red File count mismatch: missing PDF?}`);
    }
    else if (dataFileCount !== fileCount) {
      log(chalk`\t{red File count mismatch: zipped(${dataFileCount}) online(${fileCount})}`);
    }
    else {
      log(chalk`\tFile counts: {magenta match}`);
    }
    return Promise.resolve();
  });
}
