#!/usr/bin/env node

import { ApiService, ILoginContext } from '@author/api-service';
import * as fs from 'fs';
import * as https from 'https';
import { IXloYaml, LoPackageExporter, XloStateEnum } from '../index';
const lpe: LoPackageExporter = new LoPackageExporter();

/* tslint:disable:rule1 no-var-requires */

const jsyml = require('js-yaml');
const prompts = require('prompts');
const rp = require('request-promise-native');
const axios = require('axios');
const chalk = require('chalk');
const program = require('commander');
const apiSvc = new ApiService();

// tslint:disable-next-line:no-console
const log = console.log;
const safeDumpOpts = {
  skipInvalid: true,
  condenseFlow: true,
};
program
  .usage('[options] [dir]')
  .description(
    chalk`Initialize a new or empty directory with a package configuration file.  This saves host, user, and package info into a file named xlo-package.yml.  Then {magenta cd theNewPackageDir} and run {magenta xlo pull} to begin the export and packaging.`,
  )
  .option('-f, --force', 'Re-initialize the current directory and overwrite an existing xlo-package.yml file')
  .parse(process.argv);

async function GetBasicInfo() {
  const questions = [
    {
      initial: 'languageladder.author.umd.edu',
      message: 'On which website is the package located?',
      name: 'host',
      type: 'text',
    },
    {
      message: 'Username or e-mail address?',
      name: 'user',
      type: 'text',
    },
  ];
  return await prompts(questions);
}
async function PromptForPDFReactorInfo() {
  const questions = [
    {
      message: 'Please enter the PDFReactor API key',
      name: 'apiKey',
      type: 'text',
    },
    {
      message: 'Please enter the Admin Key for the conversion monitoring API',
      name: 'adminKey',
      type: 'text',
    }
  ];
  return await prompts(questions);
}

async function PromptForTargetInfo() {
  const questions = [
    {
      initial: 'languageladder.author.umd.edu',
      message: 'To which website would you like to push content?',
      name: 'host',
      type: 'text',
    },
    {
      message: 'Username or e-mail address?',
      name: 'user',
      type: 'text',
    },
    {
      message: 'Into which project (by ID#)?',
      initial: 1,
      name: 'project_Id',
      type: 'number',
    },
  ];
  return await prompts(questions);
}

async function SelectPackage(responses: IXloYaml): Promise<any> {
  const useSSL = /^local.*$/.test(responses.host || '') === false;
  const protocol = useSSL ? 'https' : 'http';
  const r: any = await prompts({
    type: 'password',
    name: 'pwd',
    message: `Enter password for user "${responses.user}"`,
  });

  const theBody: ILoginContext = {
    username: responses.user,
    password: r.pwd,
  };
  if (responses.user.indexOf('@') !== -1) {
    theBody.email = responses.user;
    delete theBody.username;
  }
  log(chalk`Attempting to login to {magenta ${protocol}://${responses.host}} ...`);
  apiSvc.setBaseUrl(`${protocol}://${responses.host}`);
  return apiSvc
    .login(theBody)
    .then((data: any) => {
      log(chalk`{magenta Successfuly logged in.}`);
      if (responses.host === 'author.nflc.umd.edu') {
        return Promise.resolve([]);
      } else {
        return apiSvc.getPackages().toPromise();
      }
    })
    .then((packages: any) => {
      const questions = [
        {
          choices: [],
          message: 'Select a package',
          name: 'package',
          type: 'select',
        },
      ];
      if (packages && packages.length) {
        const choices = [];
        for (const pkg of packages) {
          choices.push({
            title: pkg.name,
            value: JSON.stringify(pkg),
          });
        }
        questions[0].choices = choices as never[];
        return prompts(questions);
      } else {
        return Promise.resolve({
          package: JSON.stringify({
            contract: null,
            producttype: 'dlo-clo',
            filter: {},
          }),
        });
      }
    })
    .then((resp: any) => {
      const pkg = JSON.parse(resp.package);
      responses.package = {
        contract: pkg.contract,
        productType: pkg.producttype,
        filter: pkg.filter,
      };
      return Promise.resolve(responses);
    })
    .catch((error: any) => {
      log('Error--', error);
    });
}

/**
 * STARTS HERE ********************************************************
 */

let dirPath = process.cwd();
const dirName = program.args[0];
if (dirName && dirName !== '.') {
  dirPath = dirPath + '/' + dirName;
  if (fs.existsSync(dirPath)) {
    log(chalk.red('The directory path already exists: ' + dirPath));
    process.exit();
  } else {
    log('Creating directory: ', dirName);
    fs.mkdirSync(dirPath);
  }
}
log('Checking path: ', dirPath);
const state: XloStateEnum = lpe.checkDir(dirPath);
if (state === XloStateEnum.INVALIDDIR) {
  log('The specified directory is not empty.');
} else if (state === XloStateEnum.READYTOPACK && !program.force) {
  const config: IXloYaml = jsyml.safeLoad(fs.readFileSync(dirPath + '/xlo-package.yml', 'utf8'));
  log(chalk`
The specified directory is already configured as follows:

{yellow ${jsyml.safeDump(config, safeDumpOpts)}}

To download this package, type:

{magenta xlo pull}

Or, to re-initialize the current directory:

{magenta xlo init -f}
`);
} else {
  let pkgConfig: Promise<IXloYaml>;
  if (state === XloStateEnum.NOPACKAGE) {
    const config: IXloYaml = jsyml.safeLoad(fs.readFileSync(dirPath + '/xlo-package.yml', 'utf8'));
    pkgConfig = SelectPackage(config);
  } else {
    pkgConfig = GetBasicInfo()
    .then(responses => {
      return SelectPackage(responses);
    });
  }
  pkgConfig
    .then((config: IXloYaml) => {
      const yaml = jsyml.safeDump(config, safeDumpOpts);
      fs.writeFileSync(dirPath + '/xlo-package.yml', yaml, 'utf8');
      log(chalk`The {magenta xlo-package.yml} file has been written to the specified directory as follows:`);
      log(chalk`{yellow ${yaml}}`);
      log('');
      if (config.target) {
        return Promise.resolve({ doTarget: false }); // already done
      } else {
        return prompts({
          type: 'confirm',
          name: 'doTarget',
          initial: false,
          message: chalk`Would you like to add credentials for PDFReactor HTML to PDF conversions?`,
        });
      }
    })
    .then((rs: any) => {
      return rs.doTarget ? PromptForPDFReactorInfo() : Promise.resolve();
    })
    .then((rs: any) => {
      const config: IXloYaml = jsyml.safeLoad(fs.readFileSync(dirPath + '/xlo-package.yml', 'utf8'));
      if (rs) {
        config.pdfreactor = rs;
        const yaml = jsyml.safeDump(config, safeDumpOpts);
        fs.writeFileSync(dirPath + '/xlo-package.yml', yaml, 'utf8');
        log(chalk`The {magenta xlo-package.yml} file has been written to the specified directory as follows:`);
        log(chalk`{yellow ${yaml}}`);
      }
      return Promise.resolve();
    })
    .catch((error: any) => {
      if (error.name === 'StatusCodeError' && error.statusCode === 401) {
        log(chalk`{red HTTP Response: Unauthorized}`);
        log(chalk`{yellow You may have entered an incorrect username or password.}`);
      } else if (error.code && error.code === 'ENOTFOUND') {
        log(chalk`{red} Not Found: ${error.config.url}`);
      } else {
        log('Error: ', JSON.stringify(error, null, 2));
      }
      process.exit(1);
    });
}
