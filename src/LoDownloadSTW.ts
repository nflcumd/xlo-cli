import {
    ApiService,
    ILearningobjectV1p0p2,
    ILoginContext,
    IProject
} from '@author/api-service';
import FormData from 'form-data';
import fs from 'fs';
import https from 'https';
import _ from 'lodash';
import path from 'path';
import { features } from 'process';

/* tslint:disable:rule1 no-var-requires */
const jsyml = require('js-yaml');
const chalk = require('chalk');
const globy = require('globby');
const prompts = require('prompts');
const axios = require('axios');
const request = require('request');

// tslint:disable-next-line:no-console
const log = console.log;
const rejectSelfSignedCert = true;
// for axios
const agent = new https.Agent({
    rejectUnauthorized: rejectSelfSignedCert,
});
const safeDumpOpts = {
    skipInvalid: true,
    condenseFlow: true,
};

interface IFile {
    name: string;
    type: string; // file
    mtime: string; // 'Thu, 12 Aug 2021 21:33:12 GMT'
    size: number; // Kb I think
}

interface INewLoMetadata {
    id: string;
    language_Id: number;
    project_Id: number;
    product_Id: number;
    mediatype_Id: number;
}

export class LoDownloadSTW {
    private baseUrl: string = '';
    private useSSL: boolean = true;
    private wd = process.cwd();
    private accessToken: string = '';

    constructor(private apiSvc: ApiService) {

    }

    public async download(program: any) {
        const responses = await this.promptForDownloadInfo(!program.args.length);
        const objectId = program.args.length ? program.args[0] : responses.loid;
        await this.loginToSourceSystem(responses.host);
        const route = this.apiSvc.getRoutes();
        let rs;
        const pth = route.content(objectId);
        if (/^[a-zA-Z0-9_-]+$/.test(objectId)) {
            // check if object exists
            rs = await axios.head(`${this.baseUrl}${pth}`, {
                httpsAgent: agent,
            });
        } else {
            return Promise.reject(new Error('No [object-id] specified.'));
        }

        if (rs.status !== 200) {
            return Promise.reject(new Error(`Object not found with ID(${objectId}) at ${this.baseUrl}`));
        }

        // if obj exists, then make directory if no dir
        const dirPath = this.wd + '/' + objectId;
        if (!fs.existsSync(dirPath)) {
            log('Make directory: ', dirPath);
            fs.mkdirSync(dirPath);
        }
        // download via /media url
        const rs2 = await axios.get(`${this.baseUrl}${pth}`, {
            httpsAgent: agent,
        });
        // log('Data', rs2.data);
        const LO = rs2.data as ILearningobjectV1p0p2;

        // TODO: maybe do this only for AOs, Assessments have multiple sources but
        // were, I think, submitted to /content-full endpoint which allowed
        // for multiple sources.  All other types were single source anyhow.
        if (LO.product === 'AO') {
            log('AOs found.  Breaking up the content into submission parts by source.');
            const LOs = this.apiSvc.breakItUp(LO);
            if (!LOs.length) {
                log(LO);
                return Promise.reject(new Error('Invalid data found'));
            }
            // tslint:disable-next-line:prefer-for-of
            for (let x = 0; x < LOs.length; x++) {
                // fs.writeFileSync(dirPath + '/content.json', JSON.stringify(LO, null, 2), 'utf8');
                const yaml = jsyml.safeDump(LOs[x], safeDumpOpts);
                const fileName = `${dirPath}/content_${x}.yml`;
                fs.writeFileSync(fileName, yaml, 'utf8');
            }    
        } else {
            // all other types, submitted to /content-full as one doc,
            // but need to ensure the glossary items are with their source
            this.apiSvc.moveRootGlossToSources(LO);
            const yaml = jsyml.safeDump(LO, safeDumpOpts);
            const fileName = `${dirPath}/content.yml`;
            fs.writeFileSync(fileName, yaml, 'utf8');

            try {

                const metadata = await this.apiSvc.getLoMetadata(LO.id);
                const mFileName = `${dirPath}/metadata.json`;
                fs.writeFileSync(mFileName, JSON.stringify(metadata), 'utf8');    
            } catch(e) {
                log(JSON.stringify(e));
            }
        }


        if (program.media) {
            this.apiSvc.setBaseUrl(this.baseUrl);
            log(`Downloading media from:  ${this.baseUrl}${pth}`);
            return this.downloadMedia(objectId);
        }
    }

    public async saveToWeb() {
        await this.loginToTargetSystem();
        // get object to be saved to web
        // glob the local directories with content.yml files
        const files: string[] = await globy([path.join(this.wd, '*', 'content.yml')], {
            cwd: this.wd
        });
        let LO;
        const route = this.apiSvc.getRoutes();
        let objectId;
        let filePath;
        let metadata;
        if (files.length > 1) {
            filePath = await this.promptToSelectFile(files);
        }
        else if (files.length === 1) {
            filePath = files[0];
        } else {
            return Promise.reject(new Error(`No object found in local directory`));
        }
        try {
            LO = jsyml.safeLoad(fs.readFileSync(filePath, 'utf8'));
            objectId = LO.containerId;
            log('Found one object to save to the web: ', objectId);
            const mta = fs.readFileSync(`${this.wd}/${objectId}/metadata.json`, 'utf8');
            metadata = JSON.parse(mta);
            if (!metadata || !metadata.id) {
                return Promise.reject(new Error(`Could not read the metadata.json file`));                
            }

        } catch (e) {
            log(e);
            return Promise.reject(new Error(`Could not read the yaml file: ${filePath}`));
        }
        if (!objectId) {
            return Promise.reject(new Error(`Object ID could not be identified, please check the YAML file.`));
        }
        // first, check if content is still valid against schema
        const valResp = await this.apiSvc.validateLoContent(LO);
        if (!valResp.valid) {
            const errorMsg = valResp.errors.join('; ');
            const ys = await this.promptConfirm('Continue anyway?', chalk`{yellow The data file is not valid against the schema. Validation errors:\n\n ${errorMsg}}`);
            if (!ys) {
                return Promise.reject(new Error(errorMsg));
            }
        }
        // next, check if object exists in the target AUTHOR system
        const lom = await this.apiSvc.getLoMetadata(objectId);
        let project: any = {};
        let create: boolean;
        if (lom) {
            log(`An LO with matching ID: "${objectId}" was found in the target system (${this.baseUrl}).`);
            if (lom.published) {
                const erMsg = `The object is published. You must unpublish the object before you can save over it.`;
                return Promise.reject(new Error(erMsg));
            }
            project.id = lom.project_Id || -1;
            project.name = (await this.apiSvc.getProject(project.id).then(p => p.name).catch(e => 'unknown'));
            create = false;
        } else {
            log(`The LO "${objectId}" does not exists in the target system (${this.baseUrl}).`);
            // if not, then prompt for project to create object
            project = await this.promptForProject();
            create = true;
        }
        if (!project || project.id === -1) {
            log(chalk`{red Invalid project ID.}`);
            return process.exit(1);
        }
        const endpoint = await this.promptForEndpoint(LO.product, LO.containerId);
        // Please confirm host, project, object file, media files?
        const confirmationMsg = chalk`Save content from: {magenta ${filePath}}
...to endpoint: {magenta ${endpoint}}
...under project: {magenta ID:${project?.id}  Name: ${project?.name}}
...{magenta ${create ? 'creating' : 'overwriting'}} object with ID: {magenta ${objectId}}
`;
        log(' ');
        log('---------------------------------------');
        const yes = await this.promptConfirm(chalk`Please confirm the above info is correct and you would like to proceed.`, confirmationMsg);
        if (yes) {
            if (create) {
                const lang = await this.apiSvc.getLangByIso(LO.language);
                // create object shell first with required LO metadata
                delete metadata.launchURL;
                delete metadata.mediaBasePath;
                delete metadata.client;
                delete metadata.project_Id;
                delete metadata.idx;
                delete metadata.containerId;
                delete metadata.releasedate;
                metadata.project_Id = project.id;
                metadata.published = false;
                await this.apiSvc.createNewLo(metadata);
                log('Successfully created the object shell.');
            }
            const loPath = route.content(objectId);
            LO.originUri = 'xlo-stw:' + loPath.substring(0, loPath.lastIndexOf('/'));
            let validMsg;
            if (LO.product === 'AO') {
                validMsg = await this.apiSvc.saveDloContent(LO);
            } else {
                validMsg = await this.apiSvc.saveDloContentFull(LO);
            }
            if (validMsg.mediaMissing?.fromDirectory?.length) {
                log("The following files were identified as missing from the target system's object directory.");
                const pathParts = filePath.split('/');
                pathParts.pop();
                const objBasePath = pathParts.join('/');
                const foundFiles = [];
                for (const missingFile of validMsg.mediaMissing.fromDirectory) {
                    log(missingFile);
                    let found = false;
                    const localFile = `${objBasePath}/${missingFile}`;
                    try {
                        found = !!fs.statSync(localFile);
                    // tslint:disable-next-line:no-empty
                    } catch (e) { }
                    if (found) {
                        foundFiles.push(localFile);
                    }
                }
                log(`${foundFiles.length} of the ${validMsg.mediaMissing.fromDirectory.length} missing files were found in the local directory.`);
                foundFiles.map(f => log(f));
                if (foundFiles.length) {
                    const ys = await this.promptConfirm('Upload these files now?');
                    if (ys) {
                        for (const file of foundFiles) {
                            const formData = new FormData();
                            formData.append(path.basename(file), fs.createReadStream(file));
                            await this.apiSvc.upload(LO.containerId, formData)
                                .catch((e) => {
                                    log(chalk`{red Failed:} ${path.basename(file)}`);
                                    log('Try uploading the file directly through Mission Control');
                                });
                            log('uploaded: ', file);
                        }
                    }    
                }
            }
        }
    }

    private async downloadMedia(id: string) {
        log('Downloading media...');
        const route = this.apiSvc.getRoutes();
        const contentPath = route.content(id);
        const loFiles = contentPath.substring(0, contentPath.lastIndexOf('/'));
        // log('loFiles', loFiles);
        const rs = await axios.get(`${this.baseUrl}${loFiles}/`, {
            httpsAgent: agent,
        });
        // log(rs.data);
        const fileList = rs.data as IFile[];
        const dirPath = this.wd + '/' + id;
        for (const file of fileList) {
            await this.transferFile(`${this.baseUrl}${loFiles}/${file.name}`, `${dirPath}/${file.name}`);
            log(chalk`Downloaded: {magenta ${file.name}}`);
        }
    }

    private async transferFile(theUrl: string, savePath: string) {
        return new Promise((resolve, reject) => {
            request
                .get(theUrl)
                .on('error', reject)
                .pipe(fs.createWriteStream(savePath))
                .on('finish', resolve);
        });
    }

    private async promptForDownloadInfo(inclLOID = false): Promise<any> {
        const questions: any[] = [
            {
                choices: [{
                    title: 'languageladder.author.umd.edu',
                    value: 'languageladder.author.umd.edu'
                }, {
                    title: 'test.author.umd.edu',
                    value: 'test.author.umd.edu'
                }],
                message: 'From which website would you like to download the object?',
                name: 'host',
                type: 'select',
            }
        ];
        if (inclLOID) {
            questions.push({
                message: 'Learning Object ID?',
                name: 'loid',
                type: 'text',
            });
        }
        return prompts(questions);
    }

    private async promptForLoginInfo() {
        const questions = [
            {
                choices: [{
                    title: 'languageladder.author.umd.edu',
                    value: 'languageladder.author.umd.edu'
                }, {
                    title: 'test.author.umd.edu',
                    value: 'test.author.umd.edu'
                }, {
                    title: 'localhost:3001',
                    value: 'localhost:3001'
                }],
                message: 'To which AUTHOR website would you like to save the object?',
                name: 'host',
                type: 'select',
            },
            {
                message: 'Username or e-mail address?',
                name: 'user',
                type: 'text',
            },
            {
                name: 'pwd',
                message: `Enter password`,
                type: 'password',
            },
        ];
        return prompts(questions);
    }
    private async promptForCredentials() {
        const questions = [
            {
                message: 'Username or e-mail address?',
                name: 'user',
                type: 'text',
            },
            {
                name: 'pwd',
                message: `Enter password`,
                type: 'password',
            },
        ];
        return prompts(questions);
    }

    private async promptToSelectFile(files: string[]): Promise<string> {
        const choices = files.map(file => {
            return {
                title: file,
                value: file
            };
        });
        const questions = [
            {
                choices,
                message: 'Which data file would you like to save to web?',
                name: 'filePath',
                type: 'select',
            }
        ];
        return prompts(questions).then((rs: any) => rs.filePath);
    }
    private async promptForProject(): Promise<{ id: number, name: string }> {
        const projects: IProject[] = await this.apiSvc.getProjects().toPromise();
        const choices = projects.map(prj => {
            return {
                title: prj.fullname,
                value: prj.id
            };
        });
        const questions = [
            {
                choices,
                message: 'In which AUTHOR project would you like to create the object?',
                name: 'project',
                type: 'select',
            }
        ];
        return prompts(questions).then((rs: any) => {
            const obj = choices.find(o => o.value === rs.project);
            return { id: rs.project, name: obj?.title };
        });
    }
    private async promptForEndpoint(product: string, id: string): Promise<string> {
        const route = this.apiSvc.getRoutes();
        const choices = [{
            title: '/content (when content is divided among multiple files)',
            value: route.dloContent(id)
        },{
            title: '/content-full (when all content is in a single file)',
            value: route.dloContentFull(id)
        }];
        const questions = [
            {
                choices,
                message: 'To which API endpoint would you like to submit?',
                name: 'endpoint',
                type: 'select',
                initial: product === 'AO' ? 0 : 1
            }
        ];
        return prompts(questions).then((rs: any) => rs.endpoint);
    }
    private async promptConfirm(msg: string, before?: string): Promise<boolean> {
        if (before) {
            log(before);
        } else {
            log(chalk.magenta('Please Confirm...'));
        }
        const questions = [
            {
                initial: false,
                message: msg,
                name: 'confirm',
                type: 'confirm',
            }
        ];
        return prompts(questions).then((rs: any) => rs.confirm);
    }
    private async loginToSourceSystem(host: string) {
        const credsInfo: any = await this.promptForCredentials();
        const useSSL = /^local.*$/.test(host || '') === false;
        const protocol = useSSL ? 'https' : 'http';

        log(chalk`Login to {magenta ${host}} with user {magenta ${credsInfo.user}}.`);
        const theBody: ILoginContext = {
            username: credsInfo.user,
            password: credsInfo.pwd,
        };
        if (credsInfo.user.indexOf('@') !== -1) {
            theBody.email = credsInfo.user;
            delete theBody.username;
        }
        this.baseUrl = `${protocol}://${host}`;
        this.apiSvc.setBaseUrl(this.baseUrl);
        let creds;
        try {
            creds = await this.apiSvc.login(theBody);
        } catch (e) { 
            log(chalk.red('Login Failed')); 
            process.exit(1);
        }

        if (creds && creds.id) {
            log('Login token: ', creds.id);
            this.accessToken = creds.id; // for uploads
        }
    }
    private async loginToTargetSystem() {
        const srcSiteInfo: any = await this.promptForLoginInfo();
        const useSSL = /^local.*$/.test(srcSiteInfo.host || '') === false;
        const protocol = useSSL ? 'https' : 'http';

        log(chalk`Login to {magenta ${srcSiteInfo.host}} with user {magenta ${srcSiteInfo.user}}.`);
        const theBody: ILoginContext = {
            username: srcSiteInfo.user,
            password: srcSiteInfo.pwd,
        };
        if (srcSiteInfo.user.indexOf('@') !== -1) {
            theBody.email = srcSiteInfo.user;
            delete theBody.username;
        }
        this.baseUrl = `${protocol}://${srcSiteInfo.host}`;
        this.apiSvc.setBaseUrl(this.baseUrl);
        let creds;
        try {
            creds = await this.apiSvc.login(theBody);
        } catch (e) { 
            log(chalk.red('Login Failed')); 
            process.exit(1);
        }

        if (creds && creds.id) {
            log('Login token: ', creds.id);
            this.accessToken = creds.id; // for uploads
        }
    }

    // duplicated helper func from LoPackageExporter.ts
    private getContentType(file: string): string {
        if (_.endsWith(file, '.txt')) {
          return 'text/plain';
        } else if (_.endsWith(file, '.js')) {
          return 'application/javascript';
        } else if (_.endsWith(file, '.json')) {
          return 'application/json';
        } else if (_.endsWith(file, '.css')) {
          return 'text/css';
        } else if (_.endsWith(file, '.html')) {
          return 'text/html';
        } else if (_.endsWith(file, '.eot')) {
          return 'application/vnd.ms-fontobject';
        } else if (_.endsWith(file, '.svg')) {
          return 'image/svg+xml';
        } else if (_.endsWith(file, '.ttf')) {
          return 'application/font-sfnt';
        } else if (_.endsWith(file, '.woff')) {
          return 'application/font-woff';
        } else if (_.endsWith(file, '.woff2')) {
          return 'font/woff2';
        } else if (_.endsWith(file, '.otf')) {
          return 'application/font-sfnt';
        } else if (_.endsWith(file, '.png')) {
          return 'image/png';
        } else if (_.endsWith(file, '.gif')) {
          return 'image/gif';
        } else if (_.endsWith(file, '.jpg')) {
          return 'image/jpg';
        } else if (_.endsWith(file, '.mp4')) {
          return 'video/mp4';
        } else if (_.endsWith(file, '.mp3')) {
          return 'audio/mpeg';
        } else if (_.endsWith(file, '.xsd')) {
          return 'text/xml';
        }
        return 'text/plain';
      }
    
}  