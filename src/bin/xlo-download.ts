#!/usr/bin/env node

import { ApiService } from '@author/api-service';
import { LoDownloadSTW } from '../index';
const dstw: LoDownloadSTW = new LoDownloadSTW(new ApiService());

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
    .usage('[options] [object-ID]')
    .description(
        chalk`Download the data for a single learning object.`,
    )
    .option('-m, --media', 'Include the media files in the download.')
    .parse(process.argv);

    log('Include media: ', program.media);

dstw.download(program)
    .catch((error: any) => {
        if (error instanceof Error) {
            log(chalk.red(error.message));
        } else {
            log('Error: ', JSON.stringify(error, null, 2));
        }
        process.exit(1);
    });


