#!/usr/bin/env node

import { ApiService } from '@author/api-service';
// import { LoImporter, XloStateEnum } from '../index';

// const lip: LoImporter = new LoImporter(new ApiService());

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
  .description('Push local content into a remote [domain].author.umd.edu site.')
  .option('-d, --dryrun', 'This will check if the project or any of the objects exists.')
  .parse(process.argv);

// check for xlo-package.yml file to include also a project name and site, e.g.
// target:
//     host:
//     user:
//     project_Id:

// lip.checkDir().then((state: XloStateEnum) => {
//   log('state', state);
//   if (state === XloStateEnum.READYTOIMPORT) {
//     if (program.dryrun) {
//       lip.dryrun = true;
//     }
//     lip.setConfig();
    // lip
    //   .login()
    //   .then(() => {
    //     log('login complete');
    //     return lip.consumeDir();
    //   })
    //   .then(() => {
    //     log('Complete!');
    //   })
    //   .catch(e => {
    //     log('Error:', e);
    //   });
//   }
// });
