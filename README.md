# XLO CLI Tool

Is a command-line tool for exporting and packaging Learning Objects from the Author System.

## Getting Started

### Prerequisites

You can install this tool on Windows, Mac or Linux. But you must have NodeJS v18 or higher installed.  For Windows, follow the instructions below. Otherwise, go to [https://nodejs.org](https://nodejs.org) and follow the instructions.  Installing NodeJs will also install a command-line tool called `npm`, the package manager for NodeJs.

* You need to have administrative privileges on your computer.
* You need some basic familiarity with the Terminal on Mac or Command Prompt on Windows.

### Installing Node on Windows

1. [Install WSL](https://learn.microsoft.com/en-us/windows/wsl/install) on your windows machine
2. [Install node](https://learn.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-wsl) on WSL for windows.

### Installing or Updating

If you already have it installed and you just need to update it, then run `npm update -g @author/xlo`.  Otherwise, continue with the following steps:

1. Verify npm v18 or greater is installed

    ```sh
    node -v
    ```

1. Get an access token from the maintainer(s) of this NPM registry. Then copy and past the following code into a terminal.

    ```sh
    echo "//npm.author.umd.edu/:_authToken=YOUR_ACCESS_TOKEN" >> ~/.npmrc
    echo "@author:registry=https://npm.author.umd.edu/" >> ~/.npmrc
    ```

1. Install the XLO-CLI tool

    ```sh
    npm install -g @author/xlo --registry https://npm.author.umd.edu
    ```

1. Verify the installation

    ```sh
    xlo --help
    ```

## Usage

The tool has several features, but the most common use-case follows this pattern:

  1. Initializing a directory
  2. Downloading the content
  3. Formatting content for a package type
  4. Checking package integrity
  5. Generate Checksums

Note: To get help on any of the sub-commands, use `xlo <subcommand> --help`

### Initializing a directory

```sh
xlo init --help
```

When you run `xlo init NEW_FOLDER_NAME`, it first creates a new folder in the current directory.  Then, to configure the directory, you'll be prompted about the following:

* Website: If you are unsure which server to pull from, please contact the PM or AD of Products.
* Login: This is not the same as your UMD login.  You must have an admin credential which you can get from the maintainers of the Authoring System.
* Package: You must have created a "Package Definition" in Mission Control first.
* PDFReactor: HTML to PDF conversions. You will need to do this if you are planning to package with media embedded PDFs.  If unsure, contact the PM. Get the credentials from the XLO tool maintainers.

```sh
xlo init mypackage

       _                  _ _ 
 __  _| | ___         ___| (_)
 \ \/ / |/ _ \ _____ / __| | |
  >  <| | (_) |_____| (__| | |
 /_/\_\_|\___/       \___|_|_|
                              
Creating directory:  mypackage
Checking path:  /Users/myhome/Downloads/mypackage
? On which website is the package located?
✔ Username or e-mail address? myusername
✔ Enter password for user "myusername" … *************
Attempting to login to https://website.author.umd.edu
Successfuly logged in.
? Select a package › - Use arrow-keys. Return to submit.
❯   L1_German_LOs
    L3_Arabic_LOs
    L2_Hindi_LOs
    Some_Other_Pkg_etc
The xlo-package.yml file has been written to the specified directory as follows:
  ...

```

Now, run `cd mypackage` or whatever the directory name is. `cd` stands for "change directory" which moves the context of your next command into the new directory.

### Downloading the content

```sh
xlo pull --help
```

Run the following command to download the package content and corresponding user interface files.  Add the argument `-p` to generate a new PDF with the media embedded.  This will exclude the PDF without the media from the download.

```sh
xlo pull -p
```

If you added `-p` argument for the media PDFs, then you may want to follow that up by checking the status before downloading.  For large packages or if there is a backlog in the PDFReactor queue, then it can take quite a while before they're ready for download.  If you run `xlo pdf` to download them prior to being ready, the command will wait, but may timeout after about 20 minutes. So, it is better run it with the `-s` argument first, to check the status:

```sh
xlo pdf -s
[w22vzho518] conversion complete
[w22vzho519] conversion complete
```

When all objects are show as above with "conversion complete", then you can safely download the package's PDFs as follows:

```sh
xlo pdf
[w22vzho518] downloading PDF...
__CONTENT__/w22vzho518/data/w22vzho518/w22vzho518_content.pdf
[w22vzho519] downloading PDF...
__CONTENT__/w22vzho519/data/w22vzho519/w22vzho519_content.pdf
```

Before continuing, take a moment to look at the directory contents.  It should look as follows:

```text
/mypackage
  __CONTENT__  (directory)
  __UI__       (directory)
  filterList.json - contains metadata for all objects in your package
  xlo-package.yml - contains your package configuration
```

***IMPORTANT:***  If the project requires modification to the PDFs, this is where you would do that before continuing with the XLO tool steps.  Please refer to project delivery instructions before continuing.

### Formatting content for a package type

When you're ready to package you will run the `xlo pack` command which prompts you to choose a run-time environment in which these objects will be hosted, e.g. a SCORM 2004 compliant Learning Management System (LMS).  Each of these environments requires the content to be structured a bit differently.  So, after this command is run, the `__CONTENT__` directory will be modified.  So, if you need to re-do this step for any reason, you will need to delete that directory and any zip files created. Ok, go for it:

```sh
xlo pack
```

You should now see some ZIP archive files in the directory as well.  Those the packages which you can deliver to a customer for upload into their LMS.

### Checking package integrity (optional)

This next step will help ensure the quality of the deliverable.  Running the following will check each zipped package for:

* The package type (i.e. SCORM 1.2, SCORM 2004, TinCan, standalone)
* Zip archive integrity (i.e. to ensure it will open)
* File counts (i.e. compares remote count with no# of files found in the zip)

```sh
xlo check
```

***IMPORTANT:***  See project delivery instructions for details on how to organize the zip files into folders, then complete the final XLO tool step below.

### Generate Checksums (optional)

Note: Checksums will be generated automatically in the next step if using the 'xlo deliver' feature.

Run the following command to generate checksums.  By default they will be written out to a text file in the current folder.  The file name will take the parent folder name.  So, for example, if your terminal context is: `/packages/NFLC-000973/` then the text file will be: `/packages/NFLC-000973/NFLC-000973-SHA256.txt` .  If you have sub-folders under that, the command will recursively search for all `.zip` files under the current directory.  If you add more folders and zip files later, you can just re-run the command to update list. The text file outputted is a standard machine readable format.  Instructions for simple validation is written to a README file.  However, if you just want the checksums, then add the '-p' argument as shown below. They will be printed to the console and you can copy/paste from there.

```sh
xlo checksum -p
```

### Delivery

This feature enables organizing the "packages" source folder into any required folder structure required by the project, prior to zipping so that the checksum list will match the folder structure in the "delivery" destination folder.  The command `xlo deliver` will zip the content of the packages at the source into the destination, then merge the package-list.csv content into a single one, and finally take checksum values and merge that into the checksum list at the root of the delivery folder.

```yaml
packages: NFLC-000###-src (relative to the project directory)
delivery: NFLC-000### (also relative)
target: /Volumes/packages-file-share/packages/NFLC-000### (full path to smb mounted remote directory)
runtime: 2 (SCORM2004=1, SCORM1.2=2,Standalone=3)
```

Note: the **runtime** prop is used only by the `xlo pack` command to reduce the number of prompts when running large delivery projects of the same packaging type.

The **target** property is used by the `xlo deliver --copy` command to facilitate the moving of files to a mounted remote or any other target directory.  Is uses `rsync` under the hood and has params equivalent to running:

```sh
rsync -trv --timeout=60 --exclude=".DS_Store" /Volumes/NflcDrive/PortalContent/NFLC-000###/ /Volumes/packages-file-share/packages/NFLC-000###
```

Note: '-trv' is for - timestamps, recursive, verbose output.  rsync behavior here when re-running the command after a failure is to skip existing files (i.e. determined by their timestamp and file size); failed partial uploads are never actually in the destination folder because during upload they are written to a temp file in a temp folder.

## Troubleshooting

* What do I do if need to re-download the media PDFs again?
  1. No need to delete the `__CONTENT__` directory in this case.
  2. Just delete the zip files in the directory and run `xlo pull -pr`. That will skip over existing files and just re-create the PDF jobs.
  3. Then `xlo pdf -s` and `xlo pdf` when they're ready.  Note, any existing PDFs will be overwritten.
  4. Then `xlo pack` again.

* What do I do if my connection is broken while downloading content with `xlo pull` ?
  1. Delete the `__CONTENT__` and `__UI__` directories in this case.
  2. Then you can re-run `xlo pull` and continue from there.

* **Operation not permitted (MacOS)**
  This happens using `rsync` on MacOS when accessing smb:// volumes via the Terminal.
  **Solution:** Go to System Preferences → Security & Privacy → Privacy tab, select Full Disk Access, then add Terminal (or your specific shell/application). After doing so, restart Terminal and try again.

## Development

If you are working on the xlo source code, then you can install the global command as a symlink to your local working directory:

```sh
npm install -g ./
/usr/local/bin/xlo -> /usr/local/lib/node_modules/@author/xlo/lib/bin/xlo-cli.js
```

Note: @author/api-service is a dependency in this package.  To ensure future updates of the @author/api-service will pull from this registry, associate the scope with the registry as shown below.

```sh
npm login --registry=https://npm.author.umd.edu --scope=@author
```
