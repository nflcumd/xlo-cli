#!/usr/bin/env node
import { LoPackageExporter, XloStateEnum } from '../index';
const lpe: LoPackageExporter = new LoPackageExporter();
/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
  .description(
    'Download the current directory configuration.  You will be prompted for the run-time \
  environment which will determine if and how the downloaded content is packaged.',
  )
  .option('-l, --list', 'Skip the downloads and only generate the list of objects (i.e. filterList.json)')
  .option('-x, --exclude', 'Exclude the "no media" PDF from the downloads. This does not create PDFReactor jobs.')
  .option('-p, --pdfjob', 'Exclude the "no media" PDF from the downloads AND create a PDFReactor job to generate a PDF with all media embedded.')
  .option('-r, --pdfredo', "Overwrite previous PDF jobs. By default, they won't be created twice for the same package.")
  .option('-f, --force', 'Remove/overwrite any objects already downloaded.')
  .parse(process.argv);

const state: XloStateEnum = lpe.checkDir(process.cwd());

if (state !== XloStateEnum.READYTOPACK) {
  log(chalk`
You must first run:

    {magenta xlo init [dir]}
`);
} else {
  if (program.force) {
    lpe.force = true;
  }
  if (program.list) {
    lpe.listOnly = true;
  }
  if (program.exclude) {
    lpe.excludeNoMedPDF = true;
  }
  if (program.pdfjob) {
    lpe.excludeNoMedPDF = true;
    lpe.generateMediaPDF = true;
    lpe.pdfRedo = program.pdfredo;
  }
  lpe.setConfig();
  lpe
    .runXloPull()
    .then(() => {
      if (lpe.listOnly) {
        log('Package filterList.json created but no content downloaded.');
      } else {
        log('Package downloaded.');
      }
      console.log("\u0007"); console.log("\u0007");
      setTimeout(() => process.exit(), 500);
    })
    .catch(err => {
      log('Oops! Something went wrong.');
      log(err);
      process.exit(1);
    });
}
