import {
  ApiService,
  IAppProperties,
  IFilter,
  ILanguage,
  ILearningObjectMetadata,
  ILearningobjectV1p0p2,
  ILoginContext,
  IProject,
} from '@author/api-service';
import dayjs from 'dayjs';
import { SubmitOptions } from 'form-data';
import FormData from 'form-data';
import fs from 'fs';
import { IncomingMessage } from 'http';
import http from 'http';
import https from 'https';
import _ from 'lodash';
import path from 'path';
import { IXloYaml, XloStateEnum } from './LoPackageExporter';
import { LearningobjectV1p0p2 as Learningobject } from './schema_v1-0-2';

/* tslint:disable:rule1 no-var-requires */
const jsyml = require('js-yaml');
const chalk = require('chalk');
const globy = require('globby');
const prompts = require('prompts');

// tslint:disable-next-line:no-console
const log = console.log;

/**
 * Import downloaded content into another AUTHOR system.  (i.e. for xlo-push)
 */
export class LoImporter {
  public dryrun = false;
  private config: IXloYaml;
  private baseUrl: string = '';
  private useSSL: boolean = true;
  private langs: ILanguage[] = [];

  constructor(private apiSvc: ApiService, private workingDir = process.cwd(), private accessToken: string = '') {
    // default config
    this.config = {
      host: '',
      user: '',
      package: {
        contract: 'UNDEFINED',
        productType: '',
        filter: {},
      },
    };
  }

  public async PromptForLoginInfo() {
    const questions = [
      {
        initial: 'languageladder.author.umd.edu',
        message: 'To which website would you like to import from?',
        name: 'host',
        type: 'text',
      },
      {
        message: 'Username or e-mail address?',
        name: 'user',
        type: 'text',
      },
      {
        type: 'password',
        name: 'pwd',
        message: `Enter password`,
      },
    ];
    return await prompts(questions);
  }

  public async login() {
    const srcSiteInfo: any = await this.PromptForLoginInfo();
    const useSSL = /^local.*$/.test(srcSiteInfo.host || '') === false;
    const protocol = useSSL ? 'https' : 'http';

    log(chalk`Login to {magenta ${srcSiteInfo.host}} with user {magenta ${srcSiteInfo.user}}.`);
    const theBody: ILoginContext = {
      username: srcSiteInfo.user,
      password: srcSiteInfo.pwd,
    };
    if (srcSiteInfo.user.indexOf('@') !== -1) {
      theBody.email = srcSiteInfo.user;
      delete theBody.username;
    }
    this.apiSvc.setBaseUrl(`${protocol}://${srcSiteInfo.host}`);
    const creds = await this.apiSvc.login(theBody);
    if (creds && creds.id) {
      log('Login token: ', creds.id);
      this.accessToken = creds.id; // for uploads
    } else {
      log(chalk.red('Login failed'));
      this.error();
    }
  }

  public async getLoidsOfQuery(filter: IFilter): Promise<string[]> {
    const LOs = await this.apiSvc.getLearningObjects(filter);
    const loids: string[] = [];
    if (LOs && LOs.length) {
      for (const lo of LOs) {
        loids.push(lo.containerId || '0');
      }
    }
    return loids;
  }

  public getAccessToken(): string {
    return this.accessToken;
  }

  public getBaseUrl(): string {
    return this.baseUrl;
  }

  // check the working directory for some downloaded content in the expected place
  public async checkDir(): Promise<XloStateEnum> {
    const files = fs.readdirSync(this.workingDir);
    if (files.find(v => v === 'xlo-package.yml')) {
      try {
        this.config = jsyml.safeLoad(fs.readFileSync(path.join(this.workingDir, 'xlo-package.yml'), 'utf8'));
      } catch (e) {
        log(chalk.red('Could not read the xlo-package.yml file.  Please delete and re-initialize this directory.'));
        return XloStateEnum.NOCONFIG;
      }

      // validate for target and project info
      if (!this.config.hasOwnProperty('target')) {
        log(chalk.red('There was no target site identified in the xlo-package.yaml file.'));
        return XloStateEnum.NOTARGET;
      } else {
        const target = this.config.target as IXloYaml;
        if (!target.hasOwnProperty('host') || !target.hasOwnProperty('user') || !target.hasOwnProperty('project_Id')) {
          log(
            chalk.red(
              'The target (host, user, and/or project) were missing from the target property in the xlo-package.yaml file.',
            ),
          );
          return XloStateEnum.NOTARGET;
        }
      }
    }

    if (files.find(v => v === 'content') && fs.statSync(path.join(this.workingDir, 'content')).isDirectory()) {
      const cfiles = await globy(path.join(this.workingDir, 'content', '**', 'content.json'));
      if (cfiles.length) {
        return XloStateEnum.READYTOIMPORT;
      }
      log(chalk`{red Invalid or incomplete "content" directory}`);
      return XloStateEnum.INVALIDDIR;
    } else {
      log(chalk`{red No content directory found.  You may need to run xlo-pull first.}`);
      return XloStateEnum.NOCONTENT;
    }
  }

  // read xlo-package file
  public setConfig(config?: IXloYaml) {
    if (config) {
      this.config = config;
    } else {
      this.config = jsyml.safeLoad(fs.readFileSync(path.join(this.workingDir, 'xlo-package.yml'), 'utf8'));
    }
    const target = this.config.target as IXloYaml;
    const host = target.host || '';
    this.useSSL = !(/^localhost:?\d*$/.test(host) || /^0\.0\.0\.0:?\d*$/.test(host));
    const protocol = this.useSSL ? 'https' : 'http';
    this.baseUrl = `${protocol}://${target.host}`;
    this.apiSvc.setBaseUrl(this.baseUrl);
  }

  public getJsonFile(fullpath: string): any {
    return JSON.parse(fs.readFileSync(fullpath, 'utf8'));
  }

  public async pushOne(cfile: string) {
    const tmp = cfile.split(path.sep);
    tmp.pop();
    const objDirPath = tmp.join(path.sep);
    const containerId = path.basename(objDirPath);
    log(chalk`Start: {magenta ${containerId}}`);
    const loExists = await this.apiSvc.getLoExists(containerId);
    const content: ILearningobjectV1p0p2 = this.getJsonFile(cfile);
    const lang = this.langs.find(l => l.iso === content.language);
    if (lang) {
      const metadata = this.getJsonFile(path.join(objDirPath, 'metadata.json')) as ILearningObjectMetadata;
      delete metadata.idx;
      delete metadata.valid;
      delete metadata.errors;
      delete metadata.mediaBasePath;
      delete metadata.launchURL;
      metadata.published = false; // else subsequent uploads will fail
      metadata.language_Id = lang.id;
      const target = this.config.target as IXloYaml;
      const projId = parseInt(target.project_Id || '0', 10);
      metadata.project_Id = projId;
      if (!loExists) {
        await this.apiSvc.createNewLo(metadata);
      }
      this.uploadData(content);
      const files: string[] = await globy([
        path.join(objDirPath, '*'),
        '!' + path.join(objDirPath, '*.json'),
        '!' + path.join(objDirPath, '*.html'),
      ]);
      await this.uploadFiles(containerId, files);
    } else {
      log(chalk.red('Could not find corresponding language for iso locale: ', content.language));
      process.exit();
    }
    return Promise.resolve();
  }

  // glob directory and loop through content
  public async consumeDir() {
    const target = this.config.target as IXloYaml;
    // check if project exists in target site
    // const prs = await this.axi.get(`${this.baseUrl}/api/Properties`);
    const rs: [IAppProperties, ILanguage[]] = await Promise.all([
      this.apiSvc.getAppProperties(),
      this.apiSvc.getLanguages(),
    ]);
    try {
      const projects = rs[0].projects as IProject[];
      if (!projects.find(p => p.id === target.project_Id)) {
        throw new Error('project not found');
      }
    } catch (e) {
      log('Error: ', e);
      log(chalk`{red The target project_Id: ${target.project_Id} does not exist in the target site: ${target.host}}`);
      return;
    }
    this.langs = rs[1];
    // const langs = rs[1];
    const cfiles = await globy(path.join(this.workingDir, 'content', '**', 'content.json'));
    for (const cfile of cfiles) {
      await this.pushOne(cfile);
    }
  }

  private async uploadFile(containerId: string, file: string): Promise<any> {
    const target = this.config.target as IXloYaml;
    const form = new FormData();
    form.append('file', fs.createReadStream(file));
    const keepAliveAgent = new https.Agent({ keepAlive: true });
    const rOpts: https.RequestOptions = {
      method: 'post',
      host: target.host,
      port: this.useSSL ? 443 : 80,
      path: `/api/LearningObjects/${containerId}/upload`,
      headers: form.getHeaders(),
      agent: keepAliveAgent,
    };
    if (/^.*:\d+$/.test(target.host)) {
      const parts = target.host.split(':');
      rOpts.port = parseInt(parts.pop() || '', 10);
      rOpts.host = parts.pop();
    }

    return new Promise((resolve, reject) => {
      const filename = path.basename(file);
      const request = https.request(rOpts, (resp: IncomingMessage) => {
        // log(dayjs().format('hh:mm:ss'), chalk.cyan(filename), resp.statusCode, resp.statusMessage);
        resp.on('end', () => resolve({ file: filename, status: resp.statusCode, msg: resp.statusMessage }));
      });
      request.setHeader('authorization', this.accessToken);
      form.pipe(request);
      request.on('error', reject);
    });
  }

  private async uploadFiles(containerId: string, files: string[]) {
    files.map(async file => {
      return await this.uploadFile(containerId, file).then(() => {
        log(dayjs().format('hh:mm:ss'), file);
      });
    });
    log(dayjs().format('hh:mm:ss'), 'Uploads complete: ' + chalk.cyan(containerId));
    // const result = files.reduce((accumulatorPromise, file) => {
    //   return accumulatorPromise.then(rs => {
    //     log(dayjs().format('hh:mm:ss'), rs);
    //     return this.uploadFile(containerId, file);
    //   });
    // }, Promise.resolve());

    // return result.then(() => {
    //   log('All files uploaded for: ', chalk.cyan(containerId));
    // });
  }

  private async uploadData(content: Learningobject) {
    this.apiSvc
      .saveDloContentFull(content)
      .then((res: any) => {
        log(dayjs().format('hh:mm:ss'), chalk.cyan('content.json'), '200 OK');
      })
      .catch(this.error);
  }

  private sleep(millisec: number) {
    const sab = new SharedArrayBuffer(1024);
    const int32 = new Int32Array(sab);
    return Atomics.wait(int32, 0, 0, millisec);
  }

  private error(e?: Error) {
    if (e) {
      log('^^^^^^', e);
    }
    log('********');
    process.exit(99);
  }
}
