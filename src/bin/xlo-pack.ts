#!/usr/bin/env node

import { LoPackageExporter, XloStateEnum } from '../index';
const lpe: LoPackageExporter = new LoPackageExporter();

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
  .description('Pack for LMS and zip each downloaded object. Zip compression is fairly low and optimized for speed.')
  .option('-n, --nocompress', 'Do not compress the files in the zip archive at all.')
  .option('-z, --ziponly', 'Assumes contents previously packaged. This only rebuilds the ZIP archives based on the current __CONTENT__ dir.')
  .option('-s, --single', 'Zip the "content" directory as a single archive file.')
  .option('-r, --report', 'Create a report (package-list.csv), based on the package filter criteria.')
  .parse(process.argv);

const state: XloStateEnum = lpe.checkDir(process.cwd());

if (state !== XloStateEnum.READYTOPACK) {
  log(chalk`
You must first run:

    {magenta xlo init [dir]}
`);
} else {
  lpe.setConfig();
  if (program.report) {
    lpe.csv = true;
  }
  if (program.ziponly) {
    lpe.runXloPackageZip(program)
      .then(() => {
        console.log("\u0007"); console.log("\u0007");
        setTimeout(() => process.exit(), 500);
        log('Packages zipped.');
        process.exit();
      })
      .catch(err => {
        log('Oops! Something went wrong.');
        log(err);
        process.exit(1);
      });
  } else {
    lpe
      .runXloPack(program)
      .then(() => {
        // beep beep sound
        console.log("\u0007"); console.log("\u0007");
        setTimeout(() => process.exit(), 500);
      })
      .catch(err => {
        log('Oops! Something went wrong.');
        log(err);
        process.exit(1);
      });

  }
}
