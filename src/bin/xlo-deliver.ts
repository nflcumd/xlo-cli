#!/usr/bin/env node

// import devnull from 'dev-null';
import { LoPackageExporter } from '../index';
const lpe: LoPackageExporter = new LoPackageExporter();

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
  .description(
    `Build delivery folder content: zip each folder content; merge package-list.csvs; and, create a text file with a list of SHA-256 checksums.  This requires an "xlo-delivery.yml" file at the root of your delivery project and should contain properties: 

packages: NFLC-000###-src (relative to the project directory)
delivery: NFLC-000### (also relative)
target: /Volumes/packages-file-share/packages/NFLC-000### (full path to smb mounted remote directory)
runtime: 2 (SCORM2004=1, SCORM1.2=2,Standalone=3)`,
  )
  .option('-s, --strong', 'Use SHA-512 checksums instead of SHA-256')
  .option('-p, --print', 'Print to the console instead of writing text and readme file')
  .option('-c, --copy', 'Copy files to a server using rsync methods')
  .parse(process.argv);

const dir: string = process.cwd();
const errors: any = [];

(async () => {
  if (program.copy) {
    await lpe.safeSyncFileToRemote();
  } else {
    // default action
    await lpe.buildDeliveryDir();
    await lpe.buildChecksumFileList(!!program.print, !!program.strong);
  }
})()
  .then(() => {
    if (errors.length) {
      log(chalk`{red *** Errors Found ***}`);
      errors.map((x: string) => log(chalk`{red ${x}}`));
    }
    console.log("\u0007"); console.log("\u0007");
    setTimeout(() => process.exit(), 500);
  })
  .catch(err => {
    log(err);
    process.exit(1);
  });

