#!/usr/bin/env node

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

clear();
log(chalk.green(figlet.textSync('xlo-cli')));

program
  .description('A tool for exporting and packaging AUTHOR Learning Objects.')
  .command('init [dir]', 'Initialize an empty directory')
  .command('pull', 'Download content and UI and configure for a packaging type.')
  .command('pdf', 'Check status or download media embedded PDFs if PDFReactor jobs exist.')
  .command('pack', 'Pack for LMS and zip each downloaded object.')
  .command('check', 'Run an integrity check of the zip files for package.')
  .command('checksum', 'Create a text file list of SHA-256 checksums for the zip files')
  .command('download', 'Download the data for a single Learning Object.')
  .command('stw', 'Save to web, the data for a single learning object selected from a list of downloaded content in the current directory.')
  // .command('push', 'Push (i.e. import) downloaded objects into another AUTHOR system.')
  .command('version', 'Get the currently installed version of this cli tool')
  .command('deliver', 'Build delivery folder content, i.e. zip folders, merge CSVs and create checksums')
  .option('-o, --id [value]', 'Export object by id')
  .parse(process.argv);
