import { ApiService, IFilter, ILearningObjectMetadata } from '@author/api-service';
import axios, { AxiosError, AxiosResponse } from 'axios';
import * as fs from 'fs-extra';
// import * as fs from 'fs';
import * as https from 'https';
import _ from 'lodash';
import * as path from 'path';
import PDFreactor from './PDFreactor';
import * as crypto from 'crypto';
import { URL } from 'url';
import os from 'os';

const CONFIG_PATH = path.join(os.homedir(), '.xlorc');

/* tslint:disable:rule1 no-var-requires */
const jsyml = require('js-yaml');
const chalk = require('chalk');
const prompts = require('prompts');
const makeDir = require('make-dir');
const vfs = require('vinyl-fs');
const globy = require('globby');
const xml2js = require('xml2js');
const archiver = require('archiver');
const request = require('request');
const cliProgress = require('cli-progress');

// tslint:disable-next-line:no-console
const log = console.log;

const rejectSelfSignedCert = true;

// for axios
const agent = new https.Agent({
  rejectUnauthorized: rejectSelfSignedCert,
});

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export enum XloStateEnum {
  NOCONFIG = 1,
  NOPACKAGE = 2,
  READYTOPACK = 3,
  PACKAGED = 4,
  INVALIDDIR = 5,
  READYTOIMPORT = 6,
  NOTARGET = 7,
  NOCONTENT = 8,
}

export enum XloRunEnv {
  SCORM2004 = 1,
  SCORM1P2 = 2,
  STANDALONE = 3,
  NONE = 4, // i.e. data only
  TINCAN = 5,
  CMI5 = 6,
  PDF = 7,
}

export interface IXloPackage {
  contract: string;
  productType: string; // AO, VLO, DLO, DLO2, etc...
  filter: any;
}

type DeliveryConfig = {
  packages: string,
  delivery: string,
  target: string,
  runtime: XloRunEnv
}

export interface IPDFReactor {
  apiKey: string;
  adminKey: string;
}

export interface IXloYaml {
  host: string;
  user: string;
  pdfreactor?: IPDFReactor;
  project_Id?: string;
  package: IXloPackage;
  target?: IXloYaml;
}

export interface IFileInfo {
  container: string;
  name: string;
  size?: number;
  mtime?: string | Date;
}

export interface IFileListLOMetadata extends ILearningObjectMetadata {
  pdfReactorJobId?: string;
}

interface StoredConfig {
  user?: string;
  token?: string;
  userId?: number;
}

interface LoginResponse {
  id: string;
  userId: number;
}

export class LoPackageExporter {

  public force: boolean = false;
  public listOnly: boolean = false;
  public csv: boolean = false;
  public generateMediaPDF: boolean = false;
  public excludeNoMedPDF: boolean = false;
  public pdfRedo: boolean = false;
  public pdfOverwrite: boolean = false;
  private axi: any;
  private contentDirName: string = '__CONTENT__';
  private uiDirName: string = '__UI__';
  private runEnv: XloRunEnv = XloRunEnv.SCORM2004;
  private config: IXloYaml;
  private delConfig: DeliveryConfig | null = null;
  private baseUrl: string = '';
  private useSSL: boolean = true;

  constructor(
    private packageDir: string = process.cwd(),
    private accessToken: string = '',
    private langs: any[] = [],
    private filterList: any[] = [],
    private apiSvc: ApiService = new ApiService(),
  ) {
    // default config
    this.config = {
      host: '',
      user: '',
      package: {
        contract: 'UNDEFINED',
        productType: '',
        filter: {},
      },
      pdfreactor: {
        apiKey: '',
        adminKey: ''
      }
    };
  }

  // cli only
  public checkDir(dirPath: string): XloStateEnum {
    const files = fs.readdirSync(dirPath);

    if (files.find(v => v === 'xlo-package.yml')) {
      try {
        this.config = jsyml.safeLoad(fs.readFileSync('xlo-package.yml', 'utf8'));
      } catch (e) {
        log(chalk.red('Could not read the xlo-package.yml file.  Please delete and re-initialize this directory.'));
        process.exit();
      }

      // validate for package definition
      if (!this.config.package && (this.config.host && this.config.user)) {
        return XloStateEnum.NOPACKAGE;
      } else if (this.config.package && this.config.user) {
        return XloStateEnum.READYTOPACK;
      } else {
        return XloStateEnum.INVALIDDIR;
      }
    } else if (files.length === 0) {
      return XloStateEnum.NOCONFIG;
    } else {
      return XloStateEnum.INVALIDDIR;
    }
  }

  public setConfig(config?: IXloYaml) {
    if (config) {
      this.config = config;
    } else {
      this.config = jsyml.safeLoad(fs.readFileSync('xlo-package.yml', 'utf8'));
    }
    const host = this.config.host || '';
    this.useSSL = !(/^localhost:?\d*$/.test(host) || /^0\.0\.0\.0:?\d*$/.test(host));
    const protocol = this.useSSL ? 'https' : 'http';
    this.baseUrl = `${protocol}://${this.config.host}`;
  }

  public setAccessToken(token: string) {
    this.accessToken = token;
  }

  public setRunEnv(runEnv: XloRunEnv) {
    this.runEnv = runEnv;
  }

  public async login(): Promise<string | void> {
    let storedConfig: StoredConfig = {};

    // Load stored credentials if available
    if (fs.existsSync(CONFIG_PATH)) {
      try {
        storedConfig = JSON.parse(fs.readFileSync(CONFIG_PATH, 'utf-8')) as StoredConfig;
      } catch (error) {
        log(chalk.red(`Error reading config file: ${(error as Error).message}`));
      }
    }

    const username: string | undefined = this.config.user || storedConfig.user;
    const token: string | undefined = storedConfig.token;
    const userId: number | undefined = storedConfig.userId;

    if (token && username) {
      log(chalk`Checking stored credentials for {magenta ${username}}...`);

      try {
        const response: AxiosResponse = await axios.get(`${this.baseUrl}/api/users/${userId}?access_token=${token}`, {
          headers: { Authorization: token },
          httpsAgent: this.useSSL ? new (require('https').Agent)() : undefined,
        });

        if (response.status === 200) {
          log(chalk`Auto-login successful`);
          this.setAxiInstance(token);
          this.accessToken = token;
          return this.accessToken;
        }
      } catch (error) {
        console.warn(chalk.yellow('Stored token is invalid, requiring manual login.'));
      }
    }

    if (!username) {
      console.error(chalk.red('No username found in config. Please set it first.'));
      return;
    }

    log(chalk`Login to {magenta ${this.baseUrl}} with user {magenta ${username}}.`);

    // Prompt for password
    const r: { pwd: string } = await prompts({
      type: 'password',
      name: 'pwd',
      message: `Enter password`,
    });

    if (!r.pwd) {
      console.error(chalk.red('Password is required.'));
      return;
    }

    const theBody: Record<string, string> = { username, password: r.pwd };

    if (username.includes('@')) {
      theBody.email = username;
      delete theBody.username;
    }

    try {
      const response: AxiosResponse<LoginResponse> = await axios.post(
        `${this.baseUrl}/api/v2/users/login?rememberMe=false`,
        theBody,
        {
          httpsAgent: this.useSSL ? new (require('https').Agent)() : undefined,
        }
      );

      this.setAxiInstance(response.data.id);
      this.accessToken = response.data.id;

      // Save credentials to config file
      const newConfig: StoredConfig = { user: username, token: response.data.id, userId: response.data.userId };
      fs.writeFileSync(CONFIG_PATH, JSON.stringify(newConfig, null, 2), { mode: 0o600 });

      return this.accessToken;
    } catch (error) {
      console.error(chalk.red(`Login failed: ${(error as Error).message}`));
    }
  }

  public async _login() {
    log(chalk`Login to {magenta ${this.baseUrl}} with user {magenta ${this.config.user}}.`);
    const r: any = await prompts({
      type: 'password',
      name: 'pwd',
      message: `Enter password`,
    });
    const theBody: any = {
      username: this.config.user,
      password: r.pwd,
    };
    if (this.config.user.indexOf('@') !== -1) {
      theBody.email = this.config.user;
      delete theBody.username;
    }
    const response = await axios({
      method: 'post',
      url: `${this.baseUrl}/api/v2/users/login?rememberMe=false`,
      data: theBody,
      httpsAgent: this.useSSL ? agent : undefined,
    });
    this.setAxiInstance(response.data.id);
    return (this.accessToken = response.data.id);
  }

  public async setLangs() {
    const response = await axios.get(`${this.baseUrl}/api/Langs`, {
      httpsAgent: this.useSSL ? agent : undefined,
    });
    return (this.langs = response.data);
  }

  public async setLearningObjectList() {
    const url =
      `${this.baseUrl}/api/v2/LearningObjects/?filter=` +
      encodeURIComponent(JSON.stringify(this.config.package.filter));
    const data = await this.axi.get(url)
      .catch((error: AxiosError) => {
        if (error.response && error.response.status === 414) {
          const where = this.config.package.filter.where;
          if (where && where.containerId && where.containerId.inq && where.containerId.inq.length) {
            log(`Object count: ${where.containerId.inq.length}`);
            log(chalk`{yellow Warning: URI Too Long.  Attempting to chunk the object list and retry...}`);
            const chunks = _.chunk(where.containerId.inq, 100);
            const pees = [];
            for (const chunk of chunks) {
              const filt = { where: { containerId: { inq: chunk } } };
              const url2 =
                `${this.baseUrl}/api/v2/LearningObjects/?filter=` +
                encodeURIComponent(JSON.stringify(filt));
              pees.push(
                this.axi.get(url2, {
                  httpsAgent: this.useSSL ? agent : undefined,
                }),
              );
            }
            return Promise.all(pees).then((resp: AxiosResponse[]) => {
              const a = [];
              for (const rs of resp) {
                a.push(rs.data);
              }
              return _.flatten(a);
            });
          }
          return Promise.reject(
            chalk`{red HTTP Error: URI Too Long.  This is most likely because there were too many object IDs in the filter list. Try dividing the package or using filter with criteria like project and language.}`,
          );
        } else {
          return Promise.reject(new Error(error.message));
        }
      })
      .then((response: any) => {
        return response.data ? response.data : response;
      });
    this.filterList = data;
  }

  public async makeCsvFile(packageDir?: string) {
    this.apiSvc.setConfig(this.baseUrl, this.accessToken);
    const objects = await this.apiSvc.getLearningObjects(this.config.package.filter);
    const langs = await this.apiSvc.getLanguages();
    const levelmap = await this.apiSvc.getLevelMap().toPromise();
    const uniqArray = (arr: any) => [...new Set(arr)];
    const data = objects.map((lo: any) => {
      const levs = levelmap.filter(levMap => lo.levelmap_ids?.includes(levMap.id)).map(l => l.ilr);
      return {
        id: lo.containerId,
        year: new Date(lo.releasedate)?.getFullYear(),
        language: langs.find(l => l.id === lo.language_Id)?.name,
        level: uniqArray(levs).join(','),
        lessonType: lo.lessonType,
        title: lo.title,
        langId: lo.language_Id
      }
    });
    if (data.length === 0) {
      log('No data provided');
      return;
    }
    // Extract headers (keys from the first object)
    const headers = Object.keys(data[0]);
    // Convert data to CSV format
    const csvRows = [
      headers.join(','), // Header row
      ...data.map((row: any) => headers.map(field => JSON.stringify(row[field] ?? '')).join(',')) // Data rows
    ];
    // Join rows with newline character
    const csvString = csvRows.join('\n');
    const filePath = path.join(packageDir ? packageDir : this.packageDir, 'package-list.csv')
    fs.writeFileSync(filePath, csvString, { encoding: 'utf8' });
    log(chalk`Wrote: {magenta ${filePath}}`)
  }

  public getFilterListJson(): ILearningObjectMetadata[] {
    return fs.readJsonSync(path.join(this.packageDir, 'filterList.json'), { throws: false });
  }

  public async promptForRunEnv() {
    const r: any = await prompts({
      type: 'select',
      name: 'value',
      message: 'For which run-time environment would you like to package these objects?',
      choices: [
        { title: 'Scorm 2004', value: XloRunEnv.SCORM2004 },
        { title: 'Scorm 1.2', value: XloRunEnv.SCORM1P2 },
        { title: 'TinCan', value: XloRunEnv.TINCAN },
        { title: 'Web Portal or Standalone', value: XloRunEnv.STANDALONE },
        { title: 'None (i.e. data only)', value: XloRunEnv.NONE },
      ],
      initial: 0,
    });
    log('To skip this prompt next time use this value in "xlo-delivery.yml"')
    log(chalk`{magenta runtime: ${r.value}}`)
    return (this.runEnv = r.value);
  }

  public async runXloPull(): Promise<any> {
    await this.login().catch(err => {
      if (err.response.status === 401) {
        log(chalk`{red Invalid username or password}`);
      } else {
        log(err);
      }
      process.exit();
    });
    if (this.csv) {
      await this.makeCsvFile();
    } else {
      await this.createDirectoryStructure();
      // TODO: axi.get product info in order to get folder
      await this.downloadUi();
      await this.validQuery(this.config.package.filter);
      await this.downloadData();
      if (this.generateMediaPDF) {
        await this.createPDFReactorJobs();
      }
    }
    fs.writeFileSync(path.join(this.packageDir, 'filterList.json'), JSON.stringify(this.filterList, null, 2));
  }

  public getPDFReactorMonitorUrl() {
    const protocol = this.useSSL ? 'https' : 'http';
    const url = new URL(`${protocol}://${this.config.host}/pdfreactor/monitor`);
    url.searchParams.set('adminKey', `${this.config.pdfreactor?.adminKey}`);
    return url;
  }
  public async getPDFReactorClient() {
    const protocol = this.useSSL ? 'https' : 'http';
    const pdfReactor = new PDFreactor(`${protocol}://${this.config.host}/pdfreactor/rest`);
    pdfReactor.apiKey = this.config.pdfreactor?.apiKey;
    try {
      await pdfReactor.getStatus(null);
    } catch (e) {
      log(chalk`{red ${(e as Error).message}}`);
    }
    return pdfReactor;
  }
  public getPDFReactorDocConfig(url: string, titl?: string) {
    const protocol = this.useSSL ? 'https' : 'http';
    return {
      document: url,
      baseURL: `${protocol}://${this.config.host}/`,
      logLevel: PDFreactor.LogLevel.WARN,
      title: titl || '',
      author: "National Foreign Language Center",
      viewerPreferences: [
        PDFreactor.ViewerPreferences.FIT_WINDOW,
        PDFreactor.ViewerPreferences.PAGE_MODE_USE_THUMBS
      ],
      javaScriptSettings: {
        enabled: true
      },
      conversionTimeout: 240 // 4 minutes
    };
  }

  public async createPDFReactorJobs() {
    const pdfReactor = await this.getPDFReactorClient();
    const protocol = this.useSSL ? 'https' : 'http';
    const baseUrl = `${protocol}://${this.config.host}`;
    const docUrl = new URL(baseUrl);
    docUrl.searchParams.set('access_token', this.accessToken);
    docUrl.searchParams.set('media', 'true');
    // get list of objects
    let list: ILearningObjectMetadata[] = this.filterList;
    if (!list || list.length === 0) {
      list = this.getFilterListJson();
    }
    for (const lrnObj of list) {
      const lo = lrnObj as IFileListLOMetadata;
      if (lo.id) {
        docUrl.pathname = '/api/printable/' + lo.id;
        const docConfig = this.getPDFReactorDocConfig(docUrl.toString(), lo.title);
        // check if pdf already exists
        const pth = path.join(this.getContentDir(), lo.containerId || lo.id);
        if (!this.pdfRedo) {
          if (fs.existsSync(path.join(pth, 'content.pdf'))) {
            log(chalk`{magenta [${lo.containerId}] skipping PDFReactor job creation. "content.pdf" already exists.}`);
            continue;
          }
          if (lo.pdfReactorJobId) {
            log(chalk`{magenta [${lo.containerId}] skipping PDFReactor job creation. A job already exists.}`);
            continue;
          }
        }
        try {
          lo.pdfReactorJobId = await pdfReactor.convertAsync(docConfig, null);
          log(chalk` Creating PDF for {magenta ${lo.containerId}} - Job ID: {magenta ${lo.pdfReactorJobId}}`);
        } catch (e) {
          if (e instanceof PDFreactor.PDFreactorWebserviceError) {
            log(chalk`{red ${e.message}}`);
          } else {
            log(chalk`{red Unknown PDFReactor error}`);
            log(e);
          }
          return;
        }
      }
    }
  }

  public async checkPDFReactorStatus() {
    const pdfReactor = await this.getPDFReactorClient();
  }

  public async checkPDFReactorConversions() {
    const url = await this.getPDFReactorMonitorUrl();
    url.pathname += '/conversions.json';
    // https://test.author.umd.edu/pdfreactor/monitor/conversions.json?adminKey=dwifmw359vPTn7J3Cf5B
    const response = await axios({
      method: 'get',
      url: url.toString(),
      httpsAgent: this.useSSL ? agent : undefined,
    });
    const str = JSON.stringify(response.data, null, 2);
    log(chalk`{green ${str}}`);
  }

  public async checkProgressOfPdfJobsInPackage() {
    const pdfReactor = await this.getPDFReactorClient();
    // get list of objects
    const list: any = this.getFilterListJson();
    if (!list) {
      log(chalk`{red "xlo pull" must be run before this command to generate the list of objects.}`);
    }
    for (const lrnObj of list) {
      const lo = lrnObj as IFileListLOMetadata;
      if (lo.id) {
        try {
          const progress = await pdfReactor.getProgress(lo.pdfReactorJobId, null);
          if (progress.finished) {
            log(chalk`{green [${lo.id}] conversion complete}`);
            if (progress.log?.records?.length) {
              for (const lg of progress.log.records) {
                log(chalk`{yellow [${lg.levelName}] ${lg.message}}`);
              }
            }
          } else {
            log(chalk`{magenta [${lo.id}] ${progress.progress}% complete}`);
          }
        } catch (e) {
          if (e instanceof PDFreactor.PDFreactorWebserviceError) {
            log(chalk`{magenta [${lo.id}]} No PDF job found`);
          } else {
            log(e);
          }
        }
      }
    }
  }

  public async sleep(ms: number) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

  // https://support.realobjects.com/support/solutions/articles/7000045410-how-do-i-access-resources-that-are-secured-via-basic-or-digest-authentication-
  // https://www.pdfreactor.com/product/doc_html/#DocumentBundles
  // https://www.pdfreactor.com/product/doc_html/#startingAsyncConversion
  public async downloadPDFs() {
    const pdfReactor = await this.getPDFReactorClient();
    // get list of objects
    const list: any = this.getFilterListJson();
    if (!list) {
      log(chalk`{red "xlo pull" must be run before this command to generate the list of objects.}`);
    }
    for (const lrnObj of list) {
      const lo = lrnObj as IFileListLOMetadata;
      if (lo.pdfReactorJobId && lo.containerId) {
        const dataPath = this.getDataDir(lo.containerId);
        let progress;
        try {
          progress = await pdfReactor.getProgress(lo.pdfReactorJobId, null);
          let count = 0;
          while (!progress.finished) {
            if (++count > 50) { break; }
            log(chalk`{magenta [${lo.containerId}] ${progress.progress}% complete}`);
            this.sleep(500);
            progress = await pdfReactor.getProgress(lo.pdfReactorJobId, null);
          }
        } catch (e) {
          if (e instanceof PDFreactor.PDFreactorWebserviceError) {
            log(chalk`{red ${e.message}}`);
          } else {
            log(e);
          }
        }
        if (progress.finished) {
          const pdfFilePath = path.join(dataPath, lo.id + '_content.pdf');
          // remove any existing PDF
          if (fs.existsSync(pdfFilePath)) {
            await fs.unlinkSync(pdfFilePath);
          }

          log(chalk`{magenta [${lo.containerId}]} downloading PDF...`);
          const writeStream = fs.createWriteStream(pdfFilePath, { encoding: 'base64' });
          pdfReactor.getDocumentAsBinary(lo.pdfReactorJobId, writeStream, null);
          await new Promise((rs) => writeStream.on('close', rs));
          log(chalk`{magenta ${pdfFilePath}}`);

          // const doc = await pdfReactor.getDocumentAsBinary(lo.pdfReactorJobId);
          // await this.writeFileBinary(pdfFilePath, doc);
        } else {
          log(chalk`{red [${lo.containerId}] has not completed in a timely manner.  skipping}`);
        }

      } else {
        log(chalk`{magenta [${lo.id}] No PDF was generated}`);
      }
    }
  }

  public validQuery(filter: IFilter): Promise<void> {
    if (filter.where === undefined) {
      throw new Error('Invalid query: filter.where is not defined');
    }
    const where = JSON.stringify(filter.where);
    return this.axi
      .get(`${this.baseUrl}/api/v2/LearningObjects/count?where=${where}`)
      .then((rs: AxiosResponse) => {
        const loCount = parseInt(rs.data.count, 10);
        if (loCount === 0) {
          throw new Error('Invalid query: filter.where returns 0 LearningObjects.');
        } else if (!this.listOnly && loCount > 3000) {
          throw new Error(
            'Invalid query: filter.where returns more than 3000 LearningObjects.  If this was intentional, break up the package into smaller groups.',
          );
        }
      })
      .catch((error: AxiosError) => {
        if (error.response && error.response.status === 414) {
          return Promise.resolve(); // "URI Too Long" is no longer an issue
        }
        throw new Error(error.message);
      });
  }

  public isPacked(): boolean {
    for (const obj of this.filterList) {
      const pth = path.join(this.getContentDir(), obj.containerId);
      if (fs.existsSync(path.join(pth, 'index.html'))) {
        return true;
      }
    }
    return false;
  }

  public async runXloPack(program: { single: boolean; nocompress: boolean }) {
    this.filterList = require(path.join(this.packageDir, 'filterList.json'));
    if (this.isPacked()) {
      log(chalk`{red Already packaged.}  Cannot overwrite, but you can delete the zip files and "__CONTENT__" directory and retry, or zip the existing package with "xlo pack -z"`)
      return Promise.resolve();
    }
    const gotConfig = await this.loadDeliveryConfig();
    if (gotConfig && this.delConfig?.runtime) {
      switch (this.delConfig.runtime) {
        case XloRunEnv.SCORM2004:
          log(chalk`Configured runtime: {magenta SCORM 2004}`);
          this.runEnv = this.delConfig.runtime;
          break;
        case XloRunEnv.SCORM1P2:
          log(chalk`Configured runtime: {magenta SCORM 1.2}`);
          this.runEnv = this.delConfig.runtime;
          break;
        case XloRunEnv.TINCAN:
          log(chalk`Configured runtime: {magenta TinCan}`);
          this.runEnv = this.delConfig.runtime;
          break;
        case XloRunEnv.STANDALONE:
          log(chalk`Configured runtime: {magenta Standalone}`);
          this.runEnv = this.delConfig.runtime;
          break;
        case XloRunEnv.NONE:
          log(chalk`Configured runtime: {magenta Data only}`);
          this.runEnv = this.delConfig.runtime;
          break;
        default:
      }
    }
    if (!this.runEnv) {
      await this.promptForRunEnv();
    }
    switch (this.runEnv) {
      case XloRunEnv.SCORM2004:
      case XloRunEnv.SCORM1P2:
        await this.packForScorm();
        break;
      case XloRunEnv.TINCAN:
        await this.packForTinCan();
        break;
      default:
        // Web or Standalone
        if (!this.listOnly) {
          const objectList = [];
          for (const obj of this.filterList) {
            objectList.push({
              id: obj.id,
              title: obj.title,
              released: obj.releasedate,
              launch: '?' + obj.launchURL.split('?')[1],
            });
          }
          const manifestPath = path.join(this.getLouiDirPath(), 'manifest.json');
          fs.writeFileSync(manifestPath, JSON.stringify(objectList, null, 2));
          await this.packForStandalone();
        }
      // fs.copyFileSync(path.join(__dirname, 'menu.html'), this.packageDir + '/menu.html');
      // save filterList for 'xlo pack' command
    }
    await this.buildZipArchives(program.nocompress);
    log('All packages created and zipped');
  }

  public async runXloPackageZip(program: { single: boolean; nocompress: boolean }) {
    this.filterList = require(path.join(this.packageDir, 'filterList.json'));
    if (!this.isPacked()) {
      return Promise.reject(chalk`{red Not yet packaged}`);
    }

    await this.buildZipArchives(program.nocompress);
  }

  public async runXloPullNoCli(dir: string): Promise<any> {
    this.setAxiInstance(this.accessToken);
    if (this.runEnv === XloRunEnv.SCORM2004 || this.runEnv === XloRunEnv.SCORM1P2) {
      return this.packForScorm(dir);
    } else {
      await this.createDirectoryStructure(dir);
      return this.downloadData(true);
    }
  }

  public async createDirectoryStructure(dir: string = process.cwd()): Promise<any> {
    await axios.all([this.setLangs(), this.setLearningObjectList()]);
    if (!this.listOnly) {
      log('Creating directory structure...');
      this.packageDir = dir;
      const plist: any[] = [];
      this.contentDirName = '__CONTENT__';
      this.uiDirName = '__UI__';
      plist.push(makeDir(path.join(this.packageDir, this.uiDirName)));
      plist.push(makeDir(this.getLouiDirPath()));
      plist.push(makeDir(this.getPublicDirPath()));
      plist.push(makeDir(path.join(this.packageDir, this.contentDirName)));
      for (const LO of this.filterList) {
        plist.push(makeDir(this.getDataDir(LO.containerId)));
      }
      return Promise.all(plist);
    }
  }

  public async downloadData(includeMetadata: boolean = false): Promise<any> {
    log(chalk.magenta('downloadData() ....'))
    const loStuff: any[] = [];
    log(chalk.magenta('Downloading content.json and metadata. If listOnly, then data is not saved, only used for constructing fileList.json.'));
    for (const LO of this.filterList) {
      const a = (await Promise.all([
        this.axi.get(`${this.baseUrl}/api/LearningObjects/${LO.containerId}/content?exported=true&ds=true`),
        this.axi.get(
          `${this.baseUrl}/api/LearningObjects/${LO.containerId}/files${this.excludeNoMedPDF ? '' : '?includePdf=true'
          }`,
        ),
        this.axi.get(`${this.baseUrl}/api/LearningObjects/${LO.containerId}`),
      ]).catch((e: AxiosError) => {
        log(chalk`{red ${e.config.url}}`);
        log(e.message);
        if (e.response) {
          log(chalk`{red ${e.response.status} ${e.response.statusText}}`);
        }
        process.exit(1);
      })) as [AxiosResponse, AxiosResponse, AxiosResponse];
      log(chalk`{green ${a[0].statusText}} {blue ${a[0].config.url}}`);
      log(chalk`{green ${a[1].statusText}} {blue ${a[1].config.url}}`);
      log(chalk`{green ${a[2].statusText}} {blue ${a[2].config.url}}`);
      loStuff.push(a);
    }
    for (const response of loStuff) {
      const jso = response[0].data;
      const filelist1 = response[1].data;
      const metadata = response[2].data;
      if (!jso.containerId) {
        // for legacy CLOs
        jso.containerId = jso.id;
      }
      const filelist = filelist1.filter((f: any) => !/thumbnail\.png$/.test(f.name));
      log('Object #id: ', chalk.magenta(jso.containerId), ' with #', chalk.magenta(filelist.length), 'files');
      const dataPath = this.getDataDir(jso.containerId);
      const contentFilePath = path.join(dataPath, 'content.json');
      if (!this.listOnly) {
        log('Data downloaded.');
        fs.writeFileSync(contentFilePath, JSON.stringify(jso), { encoding: 'utf8' });
        if (includeMetadata) {
          fs.writeFileSync(path.join(dataPath, 'metadata.json'), JSON.stringify(metadata), { encoding: 'utf8' });
        }
      }
      const x = this.filterList.findIndex(o => o.containerId === jso.containerId);
      this.filterList[x].product = (jso.product || this.config.package.productType).toLowerCase();
      // expect to replace no media pdf with the media embedded pdf downloaded later
      this.filterList[x].fileCount = this.generateMediaPDF ? filelist.length + 1 : filelist.length;
      this.filterList[x].scripts = [];
      this.filterList[x].language = this.getLoLanguage(jso);
      try {
        for (const src of jso.sources) {
          const lang = this.langs.find(o => o.iso === src.locale);
          if (lang) {
            this.filterList[x].scripts.push(lang.script);
          }
        }
        // equiv of lodash _.uniq()
        this.filterList[x].scripts = [...new Set(this.filterList[x].scripts)];
        if (!this.listOnly) {
          const p = [];
          for (const file of filelist) {
            if (file.name) {
              if (file.name !== 'content.json') {
                p.push([jso.containerId, dataPath, file.name]);
              }
            } else {
              throw new Error(filelist);
            }
          }

          const downloads: any[] = [];
          for (const fle of p) {
            const target = path.join(fle[1], fle[2]);
            const p2: Promise<any> = this.fileExists(target).then(exists => {
              if (exists && this.force === false) {
                log(`Skip ${chalk.cyan(fle[2])}`);
                return Promise.resolve();
              } else {
                const theUrl = this.getDataEndpoint(fle[0], fle[2]);
                const savePath = path.join(fle[1], fle[2]);
                return this.download(theUrl, savePath);
              }
            });
            downloads.push(p2);
          }
          await Promise.all(downloads);
        }

        // tslint:disable-next-line:no-empty
      } catch (e) { }
    }
  }

  public async downloadUi(): Promise<any> {
    if (!this.listOnly) {
      log('Downloading UI...');
      const louiPublicFiles = await this.axi
        .get(`${this.baseUrl}/public/metadata.json`)
        .then((rs: AxiosResponse) => rs.data.map((f: any) => f.name));
      await this.downloadLouiPublicFiles(louiPublicFiles);

      let productUiFolder = 'loui';
      let productType = this.config?.package?.productType || '-';
      let m: RegExpMatchArray | null = productType.match(/^dlo(\d+)$/i);
      if (m) {
        productUiFolder = 'dlo-ui-v' + m[1];
      }
      const uiFiles = await this.axi
        .get(`${this.baseUrl}/${productUiFolder}/metadata.json`)
        .then((rs: AxiosResponse) => rs.data.map((f: any) => f.name));
      await this.downloadLouiFiles(uiFiles, productUiFolder);

      const indexHtmlPath = path.join(this.getLouiDirPath(), 'index.html');
      const indexHtml = fs.readFileSync(indexHtmlPath, 'utf8');
      if (indexHtml.indexOf('publicUpOne=true;')) {
        let replacedHtml = indexHtml.replace('publicUpOne=true;', 'publicUpOne=false;')
          .replace('var BUILD_URL = \'build.js\';', 'var BUILD_URL = window.location.origin + window.location.pathname.slice(0, window.location.pathname.lastIndexOf(\'/\'))+\'/build.js\';');
        fs.writeFileSync(indexHtmlPath, replacedHtml, 'utf8');
        log('modified index.html');
      }
    }
  }

  public async packForStandalone(): Promise<any> {
    log(chalk.magenta('Copy object directories into UI /data directory.'));
    const pathToUI = this.getLouiDirPath();
    const pees = this.filterList.map(async LO => {
      const dataDir = this.getDataDir(LO.containerId);
      await fs.copy(dataDir, path.join(pathToUI, 'data', LO.containerId));
    });
    await Promise.all(pees);
  }

  public async packForTinCan(dir?: string): Promise<any> {
    await this.copyUIBuildIntoDirs();
    log('copied build into dirs');
    await this.buildTinCanFiles();
  }

  public async packForScorm(dir?: string): Promise<any> {
    await this.copyUIBuildIntoDirs();
    log('copied build into dirs');
    await this.addScormFiles();
    log('added scorm files');
    await this.buildManifests();
  }

  public async copyUIBuildIntoDirs() {
    log(chalk.magenta('Copy UI files to object directories.'));
    const pees = this.filterList.map(async LO => {
      const pathToUI = this.getUiRootDir(LO.containerId);
      const exists = await this.fileExists(`${pathToUI}/index.html`);
      if (!exists || this.force) {
        await this.copyUiToPath(LO, pathToUI);
      }
    });
    return Promise.all(pees);
  }

  public async buildChecksumFileList(stdot: boolean, strong: boolean): Promise<void> {
    const fileRoot = this.delConfig ? this.delConfig.delivery : process.cwd();
    log(chalk`Building checksums from directory: \n  {magenta ${fileRoot}}`);
    const basename = path.basename(fileRoot);
    const fileName = `${basename}-SHA${strong ? '512' : '256'}.txt`;
    const checksumFilePath = path.join(fileRoot, fileName);

    // Create a set to hold relative paths already present in the checksum file.
    const processedFiles = new Set<string>();

    // If the checksum file exists, read and parse it.
    if (fs.existsSync(checksumFilePath)) {
      try {
        const existingContent = await fs.readFile(checksumFilePath, 'utf8');
        existingContent.split('\n').forEach((line) => {
          const trimmed = line.trim();
          if (trimmed) {
            // Expected format: "<hash>  ./<relativePath>"
            const parts = trimmed.split(/\s+/);
            if (parts.length >= 2) {
              const relPath = parts[1].startsWith('./') ? parts[1].substring(2) : parts[1];
              processedFiles.add(relPath);
            }
          }
        });
      } catch (err) {
        log(chalk`{red Error reading the checksum file: ${err}}`);
        return;
      }
    }

    // Get all .zip files under the fileRoot.
    let files: string[];
    try {
      files = await globy([path.join(fileRoot, '**/*.zip')]);
    } catch (err) {
      log(chalk`{red Error retrieving zip files: ${err}}`);
      return;
    }

    // Compute hashes only for files not already processed.
    const newHashLines: string[] = [];
    for (const file of files) {
      // Normalize relative path (using forward slashes).
      const relativePath = path.relative(fileRoot, file).replace(/\\/g, '/');
      if (processedFiles.has(relativePath)) {
        log(chalk`Exists, {yellow skipping: ${relativePath}}`);
        continue; // Skip file if already processed.
      }
      try {
        const hash = await this.getFileHash(file, strong);
        newHashLines.push(`${hash}  ./${relativePath}`);
      } catch (err) {
        log(chalk`{red Error computing hash for ${file}: ${err}}`);
      }
    }

    // If stdot is true, output the combined result to stdout.
    if (stdot) {
      let combinedOutput = '';
      if (fs.existsSync(checksumFilePath)) {
        try {
          const existingContent = await fs.readFile(checksumFilePath, 'utf8');
          combinedOutput = existingContent.trim();
        } catch (err) {
          log(chalk`{red Error reading checksum file for stdout output: ${err}}`);
        }
      }
      if (newHashLines.length > 0) {
        if (combinedOutput) {
          combinedOutput += '\n';
        }
        combinedOutput += newHashLines.join('\n');
      }
      log(chalk.yellow(`\n${combinedOutput}\n`));
    } else {
      // Append new hashes to the checksum file if it exists; otherwise, write a new file.
      if (fs.existsSync(checksumFilePath)) {
        if (newHashLines.length > 0) {
          try {
            let fileContent = await fs.readFile(checksumFilePath, 'utf8');
            if (!fileContent.endsWith('\n')) {
              fileContent += '\n';
            }
            fileContent += newHashLines.join('\n');
            await fs.writeFile(checksumFilePath, fileContent, { encoding: 'utf8' });
          } catch (err) {
            log(chalk`{red Error appending to the checksum file: ${err}}`);
          }
        }
      } else {
        try {
          const output = newHashLines.join('\n');
          await fs.writeFile(checksumFilePath, output, { encoding: 'utf8' });
        } catch (err) {
          log(chalk`{red Error writing the new checksum file: ${err}}`);
        }
      }

      // Generate README.txt with download and verification instructions.
      const readMe = `
To download, then verify the integrity of the downloaded files, follow these steps:

1. Open a terminal (Mac/Linux) or command prompt (Windows)
2. Download all files in this folder, preserving the structure:
    wget -r -np -nH --cut-dirs=1 -R "index.html*" https://author.umd.edu/packages/${basename}/
2. Navigate into the downloaded folder 
    cd ${basename} 
3. Run one of the following commands based on your system:
    - MacOS / Linux: 
        shasum -a ${strong ? '512' : '256'} -c ${fileName}   
    - Windows (with Git Bash or WSL installed):
        sha256sum -c ${fileName}
    - Windows (with GNU Core Utilities installed):
        gsha256sum -c ${fileName}   
`;
      try {
        await fs.writeFile(path.join(fileRoot, 'README.txt'), readMe, { encoding: 'utf8' });
      } catch (err) {
        console.error(`Error writing README.txt: ${err}`);
      }
    }
  }

  public async loadDeliveryConfig() {
    let filePath = null;
    let tmpPath = path.join(process.cwd(), 'xlo-delivery.yml');
    const currPathExists = await fs.pathExists(tmpPath);
    if (currPathExists) {
      filePath = tmpPath;
    } else {
      let tmp = process.cwd();
      let tmp2 = tmp.split('/');
      tmp2.pop(); // check parent folder
      const nextPath = path.join(tmp2.join('/'), 'xlo-delivery.yml');
      const nextPathExists = await fs.pathExists(nextPath);
      if (nextPathExists) {
        filePath = nextPath;
      }
    }
    if (!filePath) {
      log(chalk.red('Could not find "xlo-delivery.yml" in the current or parent directory'));
      return false;
    }
    try {
      this.delConfig = jsyml.safeLoad(fs.readFileSync(filePath, 'utf8'));
    } catch (e) {
    }
    if (!this.delConfig) {
      log(chalk.red('Could not find "xlo-delivery.yml" in the current directory'));
      process.exit();
    }
    if (!this.delConfig.delivery || !this.delConfig.packages) {
      log(chalk.red('Misconfigured "xlo-delivery.yml" file.  Must have "delivery" and "packages" folders specified'));
      process.exit();
    }
    if (/[\\]+/.test(this.delConfig.delivery) || /[\\]+/.test(this.delConfig.packages)) {
      log(chalk.red('"delivery" and "packages" must not contain slashes. They should be a folder name relative to the current directory.'));
      process.exit();
    }
    return true;
  }

  public async safeSyncFileToRemote() {
    await this.loadDeliveryConfig();
    const Rsync = require('rsync');
    const baseDir = process.cwd();
    let localPath = path.join(baseDir, this.delConfig?.delivery || 'unknown');
    const localPathExists = await fs.pathExists(localPath);
    if (!localPathExists) {
      log(chalk`Missing dir: {red ${localPath}}`);
      process.exit();
    }
    localPath = localPath + '/'; // with trailing slash to get just content
    const targetPath = this.delConfig?.target || 'dir/is/unknown';
    const targetPathExists = await fs.pathExists(targetPath);
    if (!targetPathExists) {
      log(chalk`Missing target dir: {red ${targetPath}}`);
      process.exit();
    }
    log(chalk`Source: {magenta ${localPath}}`);
    log(chalk`Target: {magenta ${targetPath}}`);

    const rsync = new Rsync()
      .flags('trv')  // timestamp, recursive, verbose output
      .set('timeout', 0) //  no timeout
      .set('partial')
      .set('append')
      .exclude('.DS_Store') // Exclude macOS metadata files
      .source(localPath)
      .destination(targetPath);

    const execRsync = () => {
      return new Promise((resolve) => {
        rsync.execute(
          (error: { message: any; }, code: any, cmd: any) => {
            if (error) {
              log(chalk`{red Error: ${error.message}}`);
              resolve(false);
            } else {
              log(`✅ Transfer complete`);
              resolve(true);
            }
          },
          (stdoutData: { toString: () => string | Buffer; }) => {
            // Capture standard output (progress updates)
            log(stdoutData.toString());
          },
          (stderrData: { toString: () => string | Buffer; }) => {
            // Capture error output (if any)
            log(stderrData.toString());
          }
        );
      });  
    }

    log(chalk.yellow(rsync.command()));
    const r: { value: string } = await prompts({
      type: 'confirm',
      name: 'value',
      message: `Proceed with above command and retry up to 10 times on fail.`,
      initial: true
    });

    if (r.value) {
      let success = await execRsync();
      let counter = 0;
      while (!success || counter < 10) {
        log(chalk`Retry attempt: {magenta ${counter + 1}}`)
        await sleep(60000); // wait for one minute
        success = await execRsync();
        counter++;
      }  
    }
  }

  /**
 * Build delivery directory:
 *  1. Collect child directory names and CSV contents.
 *  2. Zip directories if corresponding zip files do not exist.
 *  3. Combine CSV files into one file.
 */
  public async buildDeliveryDir() {
    const baseDir = process.cwd();
    await this.loadDeliveryConfig();
    if (!this.delConfig) {
      log(chalk.red('Could not find "xlo-delivery.yml" in the current directory'));
      process.exit();
    }
    const deliverySrcDir = path.join(baseDir, this.delConfig.packages);
    const packagesDirExists = await fs.pathExists(deliverySrcDir);
    if (!packagesDirExists) {
      log(chalk`Missing dir: {red ${deliverySrcDir}}`);
      process.exit();
    }
    const deliveryTargetDir = path.join(baseDir, this.delConfig.delivery);
    const deliveryDirExists = await fs.pathExists(deliveryTargetDir);
    if (!deliveryDirExists) {
      log(chalk`Missing dir: {red ${this.delConfig.delivery}}`);
      process.exit();
    }

    log(chalk`Building delivery from: {magenta ${deliverySrcDir}}`);

    const files = await globy(`${deliverySrcDir}/**/package-list.csv`);
    const csvContents = await Promise.all(
      files.map((file: string) => fs.readFile(file, 'utf8'))
    );
    const subDirs = files.map((file: string) => {
      const tmp = path.dirname(path.relative(deliverySrcDir, file));
      const tmp2 = tmp.split('/');
      const zipName = tmp2.pop();
      return { relPath: tmp2.join('/'), zipName };
    });

    // log('Packaging plan:');
    // subDirs.forEach((sd: any) => {
    //   log(chalk`{magenta ${sd.relPath}/${sd.zipName}.zip}`);
    // })

    // For each directory, create a zip file (if not already existing)
    for (const sd of subDirs) {
      const zipFilePath = path.join(deliveryTargetDir, sd.relPath, `${sd.zipName}.zip`);
      const zipFileSrcDir = path.join(deliverySrcDir, sd.relPath, sd.zipName);
      const zipExists = await fs.pathExists(zipFilePath);
      if (zipExists) {
        log(`Exists: ${zipFilePath}`)
      } else {
        log(`Building: ${zipFilePath}`)
        const parentDir = path.join(deliveryTargetDir, sd.relPath);
        await makeDir(parentDir);
        await this.zipDirectory(zipFileSrcDir, zipFilePath);
      }
    }

    // Merge CSV contents
    const combinedCsvPath = path.join(deliveryTargetDir, 'combined-package-list.csv');
    const combinedCsvData = this.combineCsvContents(csvContents);
    await fs.writeFile(combinedCsvPath, combinedCsvData, { encoding: 'utf8', flag: 'w' });
    log(chalk`Combined CSV written to {magenta ${combinedCsvPath}}`);
  }


  private async getFileHash(filePath: string, strong = false): Promise<string> {
    try {
      // Check if file is accessible
      fs.accessSync(filePath, fs.constants.R_OK);

      log(chalk`Generating hash for: {magenta ${path.basename(filePath)}}`);
      // Get total file size for progress calculations
      const stats = await fs.stat(filePath);
      const totalSize = stats.size;


      // Define threshold: only show progress bar if file is >= 1GB
      const threshold = 1 * 1024 * 1024 * 1024; // 1GB in bytes

      let progressBar: any = null;
      if (totalSize >= threshold) {
        progressBar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
        progressBar.start(totalSize, 0);
      }

      return new Promise((resolve, reject) => {
        const hash = crypto.createHash(strong ? 'sha512' : 'sha256');
        let processedBytes = 0;
        const stream = fs.createReadStream(filePath);

        stream.on('data', (data: Buffer) => {
          hash.update(data);
          if (progressBar) {
            processedBytes += data.length;
            progressBar.update(processedBytes);
          }
        });

        stream.on('end', () => {
          stream.destroy();
          if (progressBar) {
            progressBar.stop();
          }
          resolve(hash.digest('hex'));
        });

        stream.on('error', (err) => {
          if (progressBar) {
            progressBar.stop();
          }
          reject(err);
        });
      });
    } catch (err: any) {
      throw new Error(`File not accessible: ${err.message}`);
    }
  }

  private async buildZipArchives(nocompress: boolean = false): Promise<any> {
    if (this.runEnv === XloRunEnv.STANDALONE) {
      return this.buildSingleZipOfUiDirContents(nocompress).catch(e => {
        log(chalk.red('Error: '), e);
        process.exit(1);
      });
    } else {
      // first let's use parent folder name for a packaging directory.
      const parentDirName = path.basename(this.packageDir);
      const packageDirPath = path.join(this.packageDir, parentDirName);
      await makeDir(packageDirPath);
      if (this.csv) {
        await this.login();
        await this.makeCsvFile(packageDirPath);
      }
      const progressBar = new cliProgress.SingleBar({
        format: '{bar} {percentage}% | {info}',
        hideCursor: true
      }, cliProgress.Presets.shades_classic);

      const totalCount = this.filterList.length;
      let completed = 0;

      progressBar.start(totalCount, 0, { info: '' });

      for (const LO of this.filterList) {
        const zipFileName = LO.containerId + '.zip';
        const fullZipPath = path.join(packageDirPath, zipFileName);

        // Update the progress bar info with the current file
        progressBar.update(completed, { info: chalk.magenta(zipFileName) });

        const exists = await this.fileExists(fullZipPath);
        if (!exists || this.force) {
          await this.buildZip(fullZipPath, LO, nocompress).catch(e => {
            log(chalk.red('Error: '), e);
            process.exit(1);
          });
        }

        // Increase progress after completing the current file
        completed++;
      }
      progressBar.update(completed, { info: 'Completed' });
      progressBar.stop();
    }
  }
  private getContentDir(): string {
    return path.join(this.packageDir, this.contentDirName);
  }
  // i.e. after it's been copied over from _UI_ to _CONTENT_/containerId
  private getUiRootDir(containerId: string): string {
    return path.join(this.packageDir, this.contentDirName, containerId);
  }
  private getDataDir(containerId: string): string {
    // assuming SCORM package if not standalone
    switch (this.runEnv) {
      case XloRunEnv.NONE:
        return path.join(this.packageDir, this.contentDirName, containerId);
      case XloRunEnv.STANDALONE:
        return path.join(this.packageDir, this.contentDirName, containerId, 'data', containerId);
      default:
        return path.join(this.packageDir, this.contentDirName, containerId, 'data', containerId);
    }
  }
  private short(p: string) {
    return p.replace(this.packageDir, '').replace(/\/([^/]+)$/, (a, b) => '/' + chalk.magenta(b));
  }

  private async fileExists(target: string): Promise<boolean> {
    try {
      await fs.access(target, fs.constants.F_OK);
      return true;
    } catch (e) {
      return false;
    }
  }

  private async download(theUrl: string, savePath: string) {
    return new Promise((resolve, reject) => {
      request
        .get(theUrl)
        .on('error', reject)
        .pipe(fs.createWriteStream(savePath))
        .on('finish', resolve);
    }).then((arg: any) => {
      log(chalk`Complete: {magenta %s}`, this.short(savePath));
      return Promise.resolve(arg);
    });
  }

  private getDataEndpoint(containerId: string, fileName: string): string {
    if (/asset-[a-z\.]+$/.test(fileName)) {
      return `${this.baseUrl}/any/path/${fileName}`;
    } else {
      return `${this.baseUrl}/media/${containerId}/${fileName}`;
      // return `${this.baseUrl}/api/LearningObjectFC/${containerId}/download/${fileName}`;
    }
  }

  private setAxiInstance(accessToken: string) {
    const agnt = new https.Agent({
      rejectUnauthorized: rejectSelfSignedCert,
    });
    this.axi = axios.create({
      httpsAgent: this.useSSL ? agnt : undefined,
      headers: {
        authorization: accessToken,
      },
    });
  }

  private async downloadLouiPublicFiles(uiFileList: string[]) {
    const pees = uiFileList.map(async file => {
      const savePath = path.join(this.getPublicDirPath(), file);
      const exists = await this.fileExists(savePath);
      if (!exists || this.force) {
        return this.download(`${this.baseUrl}/public/${file}`, savePath);
      } else {
        log(chalk`Skip {cyan ${file}}`);
      }
    });
    return Promise.all(pees).catch((error: Error) => {
      log(chalk`{red %s: %s}`, error.name, error.message);
    });
  }

  private id() {
    return Math.random()
      .toString(36)
      .substr(2, 9);
  }

  private async downloadLouiFiles(uiFileList: string[], folder: string) {
    const pees = uiFileList.map(async file => {
      const savePath = path.join(this.getLouiDirPath(), file);
      const exists = await this.fileExists(savePath);
      if (!exists || this.force) {
        return this.download(`${this.baseUrl}/${folder}/${file}?cacheBust=${this.id()}`, savePath);
      } else {
        log(chalk`Skip {cyan ${file}}`);
      }
    });
    return Promise.all(pees).catch((error: Error) => {
      log(chalk`{red %s: %s}`, error.name, error.message);
    });
  }

  private async copyUiToPath(LO: any, pathToUI: string): Promise<any> {
    if (!LO.product) {
      throw new Error(LO.containerId + ': Could not identify product type');
    }
    if (!pathToUI) {
      throw new Error('pathToUI could not be found.');
    }

    const scriptFont: any = {
      arabic: 'NotoNaskhArabicUI-*',
      bengali: 'NotoSansBengaliUI-*',
      burmese: 'NotoSansMyanmarUI-*',
      devanagari: 'NotoSansDevanagariUI-*',
      ethiopic: 'NotoSansEthiopic-*',
      // 'georgian': '', (none yet)
      gujarati: 'NotoSansGujaratiUI-*',
      // 'gurmukhi': '', East Punjabi (none yet)
      hanji: 'NotoSansTC-*',
      'hanji-jiantizi': 'NotoSansSC-*',
      hebrew: 'NotoSansHebrew-*',
      jiantizi: 'NotoSansSC-*',
      kana: 'NotoSansCJKjp-*',
      korean: 'NotoSansKR-*',
      // 'lao': '', (none yet)
      nastaliq: 'NotoNastaliqUrdu-*',
      tamil: 'NotoSansTamilUI-*',
      thai: 'NotoSansThaiUI-*',
    };
    const prd = LO.product.toLowerCase();
    const jsBuildPath = /^dlo\d+$/.test(prd) ? 'build.js' : `build-${prd}.js`;
    const base = this.getLouiDirPath();
    const gulpsrc = [
      base + '/' + jsBuildPath,
      base + '/index.html',
      base + '/public/fonts.css',
      base + '/public/nflc-logo2.jpg',
      base + '/public/MaterialIcons-Regular*',
      base + '/public/videogular*',
      base + '/public/NotoSansUI-*',
      base + '/public/LICENSE*.txt',
    ];
    for (const script of LO.scripts) {
      if (scriptFont[script]) {
        gulpsrc.push(base + '/public/' + scriptFont[script]);
      }
    }

    if (LO.product.toUpperCase() === 'AO') {
      gulpsrc.push(base + '/public/nflc-logo2.png');
      gulpsrc.push(base + '/public/Pattern1.png');

      if (['LCR', 'LMC'].indexOf(`${LO.lessontype}`.toUpperCase()) !== -1) {
        // Listening AO
        gulpsrc.push(base + '/public/beep.mp3');
        gulpsrc.push(base + '/public/kennedy.mp3');
        gulpsrc.push(base + '/public/littlebeep.mp3');
        gulpsrc.push(base + '/public/passage*.mp3');
      } else {
        gulpsrc.push('!/videogular*');
      }
    }
    if (this.runEnv === XloRunEnv.TINCAN) {
      gulpsrc.push(base + '/public/xapiwrapper.min.js');
    }
    return new Promise<void>((resolve, reject) => {
      return (
        vfs
          .src(gulpsrc, { base })// .pipe(map((f:any) => log(f.path)))
          .pipe(vfs.dest(pathToUI))
          .on('finish', () => {
            resolve();
          })
          .on('error', reject)
      );
    });
  }

  private async addScormFiles(): Promise<any> {
    log(chalk.magenta('Copy SCORM files to object directories.'));
    let scormFiles: string;
    if (this.runEnv === XloRunEnv.SCORM2004) {
      scormFiles = `${__dirname}/scorm/2004/**`;
    } else if (this.runEnv === XloRunEnv.SCORM1P2) {
      scormFiles = `${__dirname}/scorm/1p2/**`;
    } else {
      return Promise.resolve();
    }
    const pees = this.filterList.map(async LO => {
      const pathToUI = this.getUiRootDir(LO.containerId);
      const exists = await this.fileExists(`${pathToUI}/ims_xml.xsd`);
      if (!exists || this.force) {
        await new Promise((rs, re) =>
          vfs
            .src(scormFiles)
            .pipe(vfs.dest(pathToUI))
            .on('finish', rs)
            .on('error', re),
        );
      }
    });
    return Promise.all(pees);
  }

  private getSourceInfo(contentJson: any, product: any) {
    const sourceInfo: any[] = [];
    try {
      for (let x = 0; x < contentJson.sources.length; x++) {
        let title = contentJson.sources[x].titleEnglish;
        const m = /<p>(.*?)<\/p>/g.exec(title);
        title = m ? m[1] : title;
        if (product === 'ao') {
          title = `Passage ${x + 1}: ${title}`;
        } else if (product === 'dlo-clo') {
          title = `Day ${x + 1}: ${title}`;
        }
        sourceInfo.push({ titleEnglish: title });
      }
    } catch (e) {
      // silently ignore
    }
    return sourceInfo;
  }

  private getObjectTitle(contentJson: any, sourceInfo: any): string {
    let objectTitle = 'UNDEFINED';
    const regex = /(<([^>]+)>)/gi;
    try {
      if (contentJson.title) {
        objectTitle = contentJson.title;
      } else {
        objectTitle = sourceInfo[0].titleEnglish;
      }
      objectTitle = objectTitle.replace(regex, '');
    } catch (e) {
      // silently ignore
    }
    return objectTitle;
  }

  private getModality(contentJson: any, product: string): string {
    let loModality = 'Mixed';
    if (product === 'ao') {
      const mapy: any = {
        RMC: 'Reading',
        RCR: 'Reading',
        LMC: 'Listening',
        LCR: 'Listening',
      };
      try {
        loModality = mapy[contentJson.lessonType] || 'UNIDENTIFIED';
      } catch (e) {
        log(chalk.red(e));
      }
    } else if (product === 'vlo') {
      loModality = 'Video';
    }
    return loModality;
  }

  private getLoLevel(contentJson: any): string {
    let loLevel = 'UNDEFINED';
    try {
      loLevel = contentJson.sources[0].level;
    } catch (e) {
      // silently ignore
    }
    return loLevel;
  }

  private getLoLanguage(contentJson: any): string[] {
    let loLanguage = 'UNDEFINED';
    let loLangIso6391 = null;
    try {
      loLanguage = contentJson.sources[0].language;
      loLangIso6391 = contentJson.sources[0].iso6391 || null;
    } catch (e) {
      // silently ignore
    }
    return [loLanguage, loLangIso6391];
  }

  private getLoProductInfo(products: any, product: string) {
    let loProduct;
    try {
      if (products[product]) {
        loProduct = products[product];
      } else {
        loProduct = products.dlo;
      }
    } catch (e) {
      loProduct = {
        name: 'UNDEFINDED',
        description: 'UNDEFINED',
        learningresourcetype: 'UNDEFINED',
      };
    }
    return loProduct;
  }

  private async buildManifests(): Promise<any> {
    log(chalk.magenta('Generate SCORM manifest files ...'));
    const pees = this.filterList.map(async LO => {
      const pathToUI = this.getUiRootDir(LO.containerId);
      const exists = await this.fileExists(`${pathToUI}/imsmanifest.xml`);
      if (!exists || this.force) {
        return this.buildManifest(LO);
      }
    });
    return Promise.all(pees).catch((error: Error) => {
      log(chalk`{red %s}: line[624] %s`, error.name, error.message);
      process.exit(1);
    });
  }

  private async buildManifest(LO: any): Promise<any> {
    const dataDir = this.getDataDir(LO.containerId);
    const contentJsonPath = path.join(dataDir, 'content.json');
    const contentJson: any = require(contentJsonPath);

    let product = this.config.package.productType.toLowerCase();
    const loModality = this.getModality(contentJson, product);
    const loLevel = this.getLoLevel(contentJson);
    const [loLanguage, loLangIso6391] = this.getLoLanguage(contentJson);

    const products: any = this.getProductInfo(loModality, loLanguage, loLevel);
    const productKeys = Object.keys(products);
    if (productKeys.indexOf(product) === -1) {
      product = contentJson.product.toLowerCase();
    }

    try {
      products.vlo.description = contentJson.description || contentJson.sources[0].description;
    } catch (e) {
      // silently ignore
    }
    const loProduct = this.getLoProductInfo(products, product);

    let loTopic = 'UNDEFINED';
    try {
      loTopic = contentJson.sources[0].topic;
    } catch (e) {
      // silently ignore
    }

    let d: Date = new Date();
    if (contentJson.dateInspected) {
      d = new Date(contentJson.dateInspected);
      if (isNaN(d.getTime())) {
        d = new Date();
      }
    }
    let loDateInspected: string = d.toISOString();
    loDateInspected = loDateInspected.substring(0, loDateInspected.indexOf('T'));

    const sourceInfo = this.getSourceInfo(contentJson, product);
    const objectTitle = this.getObjectTitle(contentJson, sourceInfo);

    const xmlDoc: string = await this.getImsManifest(this.getUiRootDir(contentJson.containerId), {
      version: this.runEnv === XloRunEnv.SCORM2004 ? '2004' : '1.2',
      baseUrl: this.baseUrl,
      courseId: contentJson.containerId,
      SCOtitle: 'AngularJS test',
      moduleTitle: 'AngularJS Test module',
      launchPage: `index.html?ui=${contentJson.product.toLowerCase()}&id=${contentJson.containerId}`,
      loMetadata: {
        title: objectTitle,
        product: loProduct,
        modality: loModality,
        contract: this.config.package.contract || 'UNKNOWN',
        language: loLanguage,
        langIso6391: loLangIso6391,
        topic: loTopic,
        level: loLevel,
        dateInspected: loDateInspected,
        sources: sourceInfo,
      },
      path: '',
      fileName: 'imsmanifest.xml',
    });
    const filePath = path.join(this.getUiRootDir(contentJson.containerId), 'imsmanifest.xml');
    return new Promise<void>((resolve, reject) => {
      return fs.writeFile(filePath, xmlDoc, (error: any) => {
        if (error) {
          return reject(error);
        }
        log(chalk`Created: {magenta ${this.short(filePath)}}`);
        return resolve();
      });
    });
  }

  private async getImsManifest(fileRoot: string, options: any): Promise<string> {
    // Object.assign(
    //   {
    //     version: '2004 or 1.2', // default assumes 1.2
    //     host: 'https://languageladder.author.umd.edu',
    //     courseId: 'CourseID',
    //     SCOtitle: 'SCO Title',
    //     moduleTitle: 'Module Title',
    //     launchPage: 'index.html',
    //     path: 'data',
    //     loMetadata: new Object(),
    //     fileName: 'imsmanifest.xml',
    //   },
    //   options,
    // );

    const xmlTokens: any = {
      scormType: options.version === '2004' ? 'adlcp:scormType' : 'adlcp:scormtype',
      fileArr: {
        $: {
          identifier: 'resource_1',
          type: 'webcontent',
          href: (options.path ? options.path + '/' : '').replace(/\\/g, '/') + options.launchPage,
        },
        file: [],
      },
    };
    xmlTokens.fileArr.$[xmlTokens.scormType] = 'sco';
    const files: string[] = await globy([path.join(fileRoot, '**')]);
    let pkgSize = 0;
    const formats: string[] = [];
    for (const file of files) {
      const stats: fs.Stats = fs.statSync(file);
      pkgSize += stats.size;
      formats.push(this.getContentType(file));
      const relPath = file.replace(fileRoot, '').replace(/^\//, '');
      const fObj: any = {
        file: {
          $: {
            href: ((options.path ? options.path + '/' : '') + relPath).replace(/\\/g, '/'),
          },
        },
      };
      xmlTokens.fileArr.file.push(fObj.file);
    }
    let xmlObj: any;
    const getStrNode = (txt: string) => ({
      string: [
        {
          _: txt,
          $: {
            language: 'en',
          },
        },
      ],
    });
    const getArStrNode = (txt: string) => [getStrNode(txt)];

    if (this.runEnv === XloRunEnv.SCORM2004) {
      xmlObj = Object.assign({}, require('./scorm/manifest_2004.json'));
      xmlObj.manifest.metadata[0].lom[0].general[0].identifier[0].entry[0] =
        options.baseUrl + '/api/LearningObjects/' + options.courseId;
      xmlObj.manifest.metadata[0].lom[0].general[0].title[0].string[0]._ = options.loMetadata.title;
      xmlObj.manifest.metadata[0].lom[0].general[0].description[0].string[0]._ = options.loMetadata.product.description;
      xmlObj.manifest.metadata[0].lom[0].general[0].keyword.push(getStrNode(options.loMetadata.title));
      xmlObj.manifest.metadata[0].lom[0].general[0].keyword.push(getStrNode(options.loMetadata.language));
      xmlObj.manifest.metadata[0].lom[0].general[0].keyword.push(getStrNode(options.loMetadata.product.name));
      xmlObj.manifest.metadata[0].lom[0].classification[0].keyword.push(getStrNode(options.loMetadata.language));
      xmlObj.manifest.metadata[0].lom[0].classification[0].keyword.push(getStrNode(options.loMetadata.product.name));
      xmlObj.manifest.metadata[0].lom[0].lifeCycle[0].contribute[0].date[0].dateTime[0] =
        options.loMetadata.dateInspected;
      xmlObj.manifest.metadata[0].lom[0].technical[0].format = _.uniq(formats);
      xmlObj.manifest.metadata[0].lom[0].technical[0].size = pkgSize;
      xmlObj.manifest.metadata[0].lom[0].educational[0].learningResourceType[0].value[0] = options.loMetadata.product.learningresourcetype.toLowerCase();
      if (options.loMetadata.langIso6391) {
        xmlObj.manifest.metadata[0].lom[0].educational[0].language.push(options.loMetadata.langIso6391);
      }
      xmlObj.manifest.metadata[0].lom[0].rights[0].description[0].string[0]._ = options.loMetadata.contract;

      xmlObj.manifest.metadata[0].lom[0].general[0]['c2lmd:c2lextensionmd'][0]['c2lmd:lotype'] =
        options.loMetadata.product.name;
      xmlObj.manifest.metadata[0].lom[0].general[0]['c2lmd:c2lextensionmd'][0]['c2lmd:modality'] =
        options.loMetadata.modality;
      xmlObj.manifest.metadata[0].lom[0].general[0]['c2lmd:c2lextensionmd'][0]['c2lmd:proflevel'] =
        options.loMetadata.level;
      xmlObj.manifest.metadata[0].lom[0].general[0]['c2lmd:c2lextensionmd'][0]['c2lmd:language'] =
        options.loMetadata.language;
      xmlObj.manifest.metadata[0].lom[0].general[0]['c2lmd:c2lextensionmd'][0]['c2lmd:topic'] =
        options.loMetadata.topic;
    } else {
      // http://www.imsproject.org/metadata/imsmdv1p2p1/imsmd_infov1p2p1.html
      xmlObj = Object.assign({}, require('./scorm/manifest_1p2.json'));
      xmlObj.manifest.metadata[0].lom[0].general[0].identifier =
        options.baseUrl + '/api/LearningObjects/' + options.courseId;
      xmlObj.manifest.metadata[0].lom[0].general[0].title[0].langstring[0]._ = options.loMetadata.title;
      xmlObj.manifest.metadata[0].lom[0].general[0].catalogentry[0].catalog = options.baseUrl;
      xmlObj.manifest.metadata[0].lom[0].general[0].catalogentry[0].entry[0].langstring[0]._ = options.courseId;
      xmlObj.manifest.metadata[0].lom[0].general[0].description[0].langstring[0]._ =
        options.loMetadata.product.description;
      xmlObj.manifest.metadata[0].lom[0].general[0].keyword[0].langstring[0]._ = options.loMetadata.title;
      xmlObj.manifest.metadata[0].lom[0].general[0].keyword[1].langstring[0]._ = options.loMetadata.language;
      xmlObj.manifest.metadata[0].lom[0].general[0].keyword[2].langstring[0]._ = options.loMetadata.product.name;
      xmlObj.manifest.metadata[0].lom[0].classification[0].keyword[1].langstring[0]._ = options.loMetadata.language;
      xmlObj.manifest.metadata[0].lom[0].classification[0].keyword[2].langstring[0]._ = options.loMetadata.product.name;
      xmlObj.manifest.metadata[0].lom[0].lifecycle[0].contribute[0].date[0].datetime[0] =
        options.loMetadata.dateInspected;
      xmlObj.manifest.metadata[0].lom[0].educational[0].learningresourcetype[0].value[0].langstring[0]._ =
        options.loMetadata.product.learningresourcetype;
      xmlObj.manifest.metadata[0].lom[0].rights[0].description[0].langstring[0]._ = options.loMetadata.contract;
    }
    xmlObj.manifest.resources = {
      resource: xmlTokens.fileArr,
    };
    xmlObj.manifest.$.identifier = options.courseId;
    xmlObj.manifest.organizations[0].organization[0].title = [options.loMetadata.title];
    xmlObj.manifest.organizations[0].organization[0].item[0].title = [options.loMetadata.title];

    const xmlBuilder = new xml2js.Builder();
    let xml = '';
    try {
      xml = xmlBuilder.buildObject(xmlObj);
    } catch (e) {
      log('XML Builder Error:', e);
      process.exit(1);
    }
    return Promise.resolve(xml);
  }

  private async buildTinCanFiles(): Promise<any> {
    log(chalk.magenta('Generate tincan.xml files ...'));
    const pees = this.filterList.map(async LO => {
      const pathToUI = this.getUiRootDir(LO.containerId);
      const exists = await this.fileExists(`${pathToUI}/tincan.xml`);
      if (!exists || this.force) {
        return this.buildTinCanFile(LO);
      }
    });
    return Promise.all(pees).catch((error: Error) => {
      log(chalk`{red %s}: %s`, error.name, error.message);
      process.exit(1);
    });
  }

  private async buildTinCanFile(LO: any): Promise<any> {
    const dataDir = this.getDataDir(LO.containerId);
    const contentJsonPath = path.join(dataDir, 'content.json');
    const contentJson: any = require(contentJsonPath);
    let product = this.config.package.productType.toLowerCase();
    const loModality = this.getModality(contentJson, product);
    const loLevel = this.getLoLevel(contentJson);
    const [loLanguage, loLangIso6391] = this.getLoLanguage(contentJson);
    const products: any = this.getProductInfo(loModality, loLanguage, loLevel);
    const productKeys = Object.keys(products);
    if (productKeys.indexOf(product) === -1) {
      product = contentJson.product.toLowerCase();
    }
    const loProduct = this.getLoProductInfo(products, product);
    const sourceInfo = this.getSourceInfo(contentJson, product);
    const objectTitle = this.getObjectTitle(contentJson, sourceInfo);
    const tinCanXml = `
<?xml version="1.0" encoding="utf-8" ?>
<tincan xmlns="http://projecttincan.com/tincan.xsd">
    <activities>
        <activity id="${this.baseUrl}/api/LearningObjects/${contentJson.containerId
      }" type="http://adlnet.gov/expapi/activities/course">
            <name>${objectTitle}</name>
            <description lang="en-US">${loProduct.description}</description>
            <launch lang="en-us">index.html?ui=${contentJson.product.toLowerCase()}&amp;id=${contentJson.containerId
      }</launch>
        </activity>
    </activities>
</tincan>
`;
    const tinCanJs = `
TC_COURSE_ID = "${this.baseUrl}/api/LearningObjects/${contentJson.containerId}";
TC_COURSE_NAME = {"en-US": "${objectTitle}"};
TC_COURSE_DESC = {"en-US": "${loProduct.description}"};
TC_RECORD_STORES = [];
`;
    const tcXmlfilePath = path.join(this.getUiRootDir(contentJson.containerId), 'tincan.xml');
    const tcJsfilePath = path.join(this.getUiRootDir(contentJson.containerId), 'tc-config.js');
    const pees = [];
    pees.push(
      new Promise<void>((resolve, reject) => {
        return fs.writeFile(tcXmlfilePath, tinCanXml, (error: any) => {
          if (error) {
            return reject(error);
          }
          log(chalk`Created: {magenta ${this.short(tcXmlfilePath)}}`);
          return resolve();
        });
      }),
    );
    pees.push(
      new Promise<void>((resolve, reject) => {
        return fs.writeFile(tcJsfilePath, tinCanJs, (error: any) => {
          if (error) {
            return reject(error);
          }
          log(chalk`Created: {magenta ${this.short(tcJsfilePath)}}`);
          return resolve();
        });
      }),
    );
    return Promise.all(pees);
  }

  private async buildSingleZipOfUiDirContents(nocompress: boolean = false) {
    const zipName = 'content.zip';
    const output = fs.createWriteStream(path.join(this.packageDir, zipName));
    const zlib = require('zlib');
    if (zlib.constants) {
      // https://nodejs.org/api/zlib.html#zlib_zlib_constants
      const compression = nocompress ? zlib.constants.Z_NO_COMPRESSION : zlib.constants.Z_BEST_SPEED;
      const archive = archiver('zip', { zlib: { level: compression }, });
      return new Promise<void>((resolve, reject) => {
        output.on('close', () => {
          log('Created: ' + zipName);
          resolve();
        });
        archive.on('warning', (err: any) => {
          if (err.code === 'ENOENT') {
            log(chalk.yellow('Warning: '), err);
          } else {
            reject(err);
          }
        });
        archive.on('error', (err: any) => {
          reject(err);
        });
        archive.pipe(output);
        archive.glob('**/*', {
          cwd: this.getLouiDirPath(),
          ignore: ['**/.DS_Store']
        });
        archive.finalize();
      });
    } else {
      return Promise.reject(
        new Error(
          'zlib.constants undefined.  This may be a result of using an old version of NodeJS. XLO requires Node 10 or above',
        ),
      );
    }
  }

  /**
 * Zips the given directory.
 *
 * @param sourceDir The directory to zip.
 * @param outPath The output zip file path.
 */
  private async zipDirectory(sourceDir: string, outPath: string): Promise<void> {
    const zlib = require('zlib');
    if (!zlib.constants) throw new Error('No zlib constants');

    // create a new progress bar instance and use shades_classic theme
    const progressBar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
    let totalEntries = 0;

    return new Promise((resolve, reject) => {
      const output = fs.createWriteStream(outPath);
      // Since we're zipping a folder full of zip files, let's assume the 
      // individual LO zips are already compressed.
      const archive = archiver('zip', {
        zlib: { level: zlib.constants.Z_NO_COMPRESSION },
      });

      archive.glob('**/*', {
        cwd: sourceDir,
        ignore: ['**/.DS_Store'],
      }, { prefix: path.basename(sourceDir) });

      output.on('close', () => {
        // stop the progress bar
        progressBar.stop();
        log(chalk`Created zip: {magenta ${outPath}} (${archive.pointer()} total bytes)`);
        resolve();
      });

      archive.on('warning', (err: any) => {
        if (err.code === 'ENOENT') {
          log(chalk.yellow('Warning: '), err);
        } else {
          reject(err);
        }
      });

      archive.on('progress', (progress: any) => {
        if (!totalEntries) {
          totalEntries = progress.entries.total;
          progressBar.start(totalEntries, 0);
        } else {
          progressBar.update(progress.entries.processed);
        }
        // log(`Processed ${progress.entries.processed} of ${progress.entries.total} entries.`);
      });

      archive.on('error', (err: any) => reject(err));

      archive.pipe(output);
      // Add the entire directory; the second parameter true means that the directory 
      // itself is included in the archive
      // archive.directory(sourceDir, true);
      archive.finalize();
    });
  }

  private async buildZip(fullZipPath: string, LO: any, nocompress: boolean = false): Promise<any> {
    const output = fs.createWriteStream(fullZipPath);
    const zlib = require('zlib');
    if (zlib.constants) {
      // https://nodejs.org/api/zlib.html#zlib_zlib_constants
      const compression = nocompress ? zlib.constants.Z_NO_COMPRESSION : zlib.constants.Z_BEST_SPEED;
      const archive = archiver('zip', {
        zlib: { level: compression },
      });

      return new Promise<void>((resolve, reject) => {
        output.on('close', resolve);
        archive.on('warning', (err: any) => {
          if (err.code === 'ENOENT') {
            log(chalk.yellow('Warning: '), err);
          } else {
            reject(err);
          }
        });
        archive.on('error', reject);
        archive.pipe(output);
        archive.glob('**/*', {
          cwd: this.getUiRootDir(LO.containerId),
          ignore: ['**/.DS_Store']
        });
        archive.finalize();
      });
    } else {
      return Promise.reject(
        new Error(
          'zlib.constants undefined.  This may be a result of using an old version of NodeJS. XLO requires Node 10 or above',
        ),
      );
    }
  }

  private async zipContentDir(): Promise<any> {
    const dir = this.getContentDir();
    const output = fs.createWriteStream(path.join(this.packageDir, path.basename(dir) + '.zip'));
    const zlib = require('zlib');
    if (zlib.constants) {
      // https://nodejs.org/api/zlib.html#zlib_zlib_constants
      const archive = archiver('zip', {
        zlib: { level: zlib.constants.Z_BEST_COMPRESSION }, // Sets the compression level.
        // zlib: { level: zlib.constants.Z_NO_COMPRESSION }
      });
      return new Promise<void>((resolve, reject) => {
        output.on('close', () => {
          log('Created: ' + path.basename(dir) + '.zip');
          resolve();
        });
        archive.on('warning', (err: any) => {
          if (err.code === 'ENOENT') {
            log(chalk.yellow('Warning: '), err);
          } else {
            reject(err);
          }
        });
        archive.on('error', (err: any) => {
          reject(err);
        });
        archive.pipe(output);
        archive.directory(dir, false);
        archive.finalize();
      });
    } else {
      return Promise.reject(
        new Error(
          'zlib.constants undefined.  This may be a result of using an old version of NodeJS. XLO requires Node 10 or above',
        ),
      );
    }
  }

  private getLouiDirPath(): string {
    return path.join(this.packageDir, this.uiDirName);
  }
  private getPublicDirPath(): string {
    return path.join(this.getLouiDirPath(), 'public');
  }

  private writeFile(savePath: string, data: string): Promise<any> {
    return new Promise<void>((resolve, reject) => {
      return fs.writeFile(savePath, data, (error: any) => {
        if (error) {
          log(chalk`{red writeFileError: %s}`, this.short(savePath));
          return reject(error);
        }
        log(chalk`Created: {magenta ${this.short(savePath)}}`);
        return resolve();
      });
    });
  }

  private writeFileBinary(savePath: string, data: string): Promise<any> {
    return new Promise<void>((resolve, reject) => {
      return fs.writeFile(savePath, data, 'binary', (error: any) => {
        if (error) {
          log(chalk`{red writeFileError: %s}`, this.short(savePath));
          return reject(error);
        }
        log(chalk`Created: {magenta ${this.short(savePath)}}`);
        return resolve();
      });
    });
  }

  private getContentType(file: string): string {
    if (_.endsWith(file, '.txt')) {
      return 'text/plain';
    } else if (_.endsWith(file, '.js')) {
      return 'application/javascript';
    } else if (_.endsWith(file, '.json')) {
      return 'application/json';
    } else if (_.endsWith(file, '.css')) {
      return 'text/css';
    } else if (_.endsWith(file, '.html')) {
      return 'text/html';
    } else if (_.endsWith(file, '.eot')) {
      return 'application/vnd.ms-fontobject';
    } else if (_.endsWith(file, '.svg')) {
      return 'image/svg+xml';
    } else if (_.endsWith(file, '.ttf')) {
      return 'application/font-sfnt';
    } else if (_.endsWith(file, '.woff')) {
      return 'application/font-woff';
    } else if (_.endsWith(file, '.woff2')) {
      return 'font/woff2';
    } else if (_.endsWith(file, '.otf')) {
      return 'application/font-sfnt';
    } else if (_.endsWith(file, '.png')) {
      return 'image/png';
    } else if (_.endsWith(file, '.gif')) {
      return 'image/gif';
    } else if (_.endsWith(file, '.jpg')) {
      return 'image/jpg';
    } else if (_.endsWith(file, '.mp4')) {
      return 'video/mp4';
    } else if (_.endsWith(file, '.mp3')) {
      return 'audio/mpeg';
    } else if (_.endsWith(file, '.xsd')) {
      return 'text/xml';
    }
    return 'text/plain';
  }

  private getProductInfo(loModality: string, loLanguage: string, loLevel: string) {
    // description must be XML compliant; i.e. no &<>
    return {
      ao: {
        name: 'Assessment Object',
        description: `This Assessment Object will help you assess your ${loModality.toLowerCase()} comprehension in ${loLanguage}.  The passages and questions are appropriate for ILR Level ${loLevel}.`,
        learningresourcetype: 'Self Assessment',
      },
      vlo: {
        name: 'Video Learning Object',
        description: `This Video Learning Object will help you improve comprehension in ${loLanguage}.  The source material and activities are appropriate for ILR Level ${loLevel}.`,
        learningresourcetype: 'Exercise',
      },
      'dlo-clo': {
        name: 'Compact Learning Object',
        description: `This Compact Learning Object will help you improve comprehension in ${loLanguage}.  The source materials are appropriate for ILR Level ${loLevel}.`,
        learningresourcetype: 'Exercise',
      },
      'dlo-eola': {
        name: 'Assessment',
        description: `This assessment will help you assess your comprehension in ${loLanguage}.`,
        learningresourcetype: 'Self Assessment',
      },
      dlo: {
        name: 'Lesson',
        description: `This lesson will help you improve comprehension in ${loLanguage}.  It is appropriate for ILR Level ${loLevel}.`,
        learningresourcetype: 'Exercise',
      }
    };
  }

  private filterInvalidXml(str: string) {
    return str
      .replace(/&/g, '&amp;')
      .replace(/'/g, '&apos;')
      .replace(/"/g, '&quot;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;');
  }

  /**
   * Combines multiple CSV strings into one.
   * It keeps the header (first line) from the first CSV and appends the data rows from all files.
   *
   * @param csvFiles Array of CSV file contents as strings.
   * @returns A single CSV string.
   */
  private combineCsvContents(csvFiles: string[]): string {
    let headerLine: string | null = null;
    const dataRows: string[] = [];

    for (const csvData of csvFiles) {
      // Split into lines and remove empty lines
      const lines = csvData.split(/\r?\n/).filter((line) => line.trim() !== '');
      if (lines.length === 0) {
        continue;
      }
      // Use the header from the first CSV file encountered
      if (!headerLine) {
        headerLine = lines[0];
      }
      // Add the rest of the rows (assume first row is the header)
      dataRows.push(...lines.slice(1));
    }

    // Build the final CSV: header line + all data rows
    return headerLine ? `${headerLine}\n${dataRows.join('\n')}` : dataRows.join('\n');
  }
}

export const lpe = LoPackageExporter;
