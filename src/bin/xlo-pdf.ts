#!/usr/bin/env node

import { LoPackageExporter, XloStateEnum } from '../index';
const lpe: LoPackageExporter = new LoPackageExporter();

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
  .description(
    'The default behavior of `xlo pdf` will download the PDFs for this package if any exist.',
  )
  .option('-s, --status', 'Check the status of jobs in the current package.')
  .option('-a, --statusall', 'Check the status of all PDFReactor conversion jobs. For debugging only.')
  .parse(process.argv);

const state: XloStateEnum = lpe.checkDir(process.cwd());

if (state !== XloStateEnum.READYTOPACK) {
  log(chalk`
You must first run:

    {magenta xlo init [dir]}
`);
} else {
  lpe.setConfig();
  if (program.status) {
    lpe.checkProgressOfPdfJobsInPackage()
      .then(() => process.exit())
      .catch(() => process.exit(1));
  }
  else if (program.statusall) {
    // lpe.checkPDFReactorStatus();
    lpe.checkPDFReactorConversions();
  } else {
    lpe
    .downloadPDFs()
    .then(() => {
      process.exit();
    })
    .catch(err => {
      log('Oops! Something went wrong.');
      log(err);
      process.exit(1);
    });
  }
}
