#!/usr/bin/env node

import { ApiService } from '@author/api-service';
import { LoDownloadSTW } from '../index';
const dstw: LoDownloadSTW = new LoDownloadSTW(new ApiService());

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
    .usage('')
    .description(
        chalk`Save to web, the data for a single learning object selected from a list of downloaded content in the current directory.`,
    )
    .parse(process.argv);

dstw.saveToWeb()
    .catch((error: any) => {
        if (error instanceof Error) {
            log(chalk.red(error.message));
        } else {
            log('Error: ', JSON.stringify(error, null, 2));
        }
        process.exit(1);
    });


