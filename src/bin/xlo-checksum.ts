#!/usr/bin/env node

// import devnull from 'dev-null';
import { LoPackageExporter } from '../index';

const lpe: LoPackageExporter = new LoPackageExporter();

/* tslint:disable:rule1 no-var-requires */

const chalk = require('chalk');
const program = require('commander');

// tslint:disable-next-line:no-console
const log = console.log;

program
  .description(
    'Create a text file with a list of SHA-256 checksums',
  )
  .option('-s, --strong', 'Create SHA-512 checksums')
  .option('-p, --print', 'Print to the console instead of writing text and readme file')
  .parse(process.argv);

const dir: string = process.cwd();
const errors: any = [];

(async () => {
  await lpe.buildChecksumFileList(!!program.print, !!program.strong);
})()
.then(() => {
    if (errors.length) {
      log(chalk`{red *** Errors Found ***}`);
      errors.map((x: string) => log(chalk`{red ${x}}`));
    }
    process.exit();
  })
  .catch(err => {
    log(err);
    process.exit(1);
  });

