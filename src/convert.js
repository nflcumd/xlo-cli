var fs = require('fs'),
  xml2js = require('xml2js');

var parser = new xml2js.Parser();
fs.readFile(__dirname + '/scorm/imsmanifest.xml', function(err, data) {
  parser.parseString(data, function(err, result) {
    //console.dir(result);
    let data = JSON.stringify(result, null, 2);
    data = data.replace(/\\n[\\t]+/, '');
    fs.writeFileSync(__dirname + '/scorm/tmp.json', data);
    console.log('Done');
  });
});
